<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Pregunta_imagen extends Model
{
    protected $table='preguntas_imagenes';
    //atributes
    
    protected $fillable = [
        'name', 'formato',"ruta", "descripcion"
    ];
    public function preguntas()
    {
        return $this->belongsTo('App\models\Pregunta');
    }
}

