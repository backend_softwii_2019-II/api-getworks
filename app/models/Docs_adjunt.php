<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Docs_adjunt extends Model
{
    protected $table='docs_adjunts';
    //atributes
    
    protected $fillable = [
        'nombre', 'tipoDocumento', 'formato',"rutaDocumento","descripcion"
    ];
    public function Aspirante()
    {
        return $this->belongsTo('App\models\Aspirante');
    }
    
    public function organizacion()
    {
        return $this->belongsTo('App\models\organizacion');
    }
}
