<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Pruebas_cuestionario extends Model
{
    protected $table='Pruebas_cuestionarios';
    //atributes
    
    protected $fillable = [
        'tipo_prueba'
    ];


    //relacion de muchos a muchos
    
    public function preguntas(){
        return $this->belongsToMany('App\models\Pregunta');
    }
    //relaciones de 1 a mucho
    public function cuestionarios_encuestas(){
        return $this->hasMany('App\models\Cuestionario_encuesta');
    }
    public function cuestionarios_psicotecnicas(){
        return $this->hasMany('App\models\Cuestionario_psicotecnica');
    }

    //relaciones inversas 
    public function Pruebas(){
        return $this->belongsTo('App\models\Prueba');
    }
}

