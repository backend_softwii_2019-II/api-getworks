<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistorialExperienciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_experiencias', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('cargo')->nullable();
            $table->string('nombre_organizacion')->nullable();
            $table->string('duracion_empleo')->nullable();
            $table->string('fecha_registro')->nullable();
            $table->string('descripcion')->nullable();
            $table->unsignedInteger('aspirantes_id');
            $table->foreign('aspirantes_id')->references('id')->on('aspirantes')->onDelete('restrict')->onUpdate('restrict');
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial_experiencias');
    }
}
