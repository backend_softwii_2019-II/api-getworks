<?php

use App\Http\Controllers\CargoController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::resource('Categoria', 'CategoriaController')->except([ 'create', 'edit']);
route::get('Categoria/cargo/{id}','CategoriaController@MostrarCargos');
Route::resource('Organizacion', 'OrganizacionController')->except([ 'create', 'edit']);
Route::post('Organizacion/login', 'OrganizacionController@iniciar');

Route::resource('User', 'UserController')->except([ 'create', 'edit']);
Route::post('User/signin', 'UserController@signin');
Route::post('User/login', 'UserController@login');
Route::post('User/upload', 'UserController@upload');
Route::get('User/avatar/{id}', 'UserController@getimage');


Route::resource('Aspirante', 'AspiranteController')->except([ 'create', 'edit']);
Route::resource('Academico', 'NivelAcademicoController')->except([ 'create', 'edit']);
Route::resource('Experiencia', 'NivelExperienciaController')->except([ 'create', 'edit']);


Route::resource('Apuntado_evaluado', 'Apuntado_evaluadoController')->except([ 'create', 'edit']);


Route::resource('Cargo', 'CargoController')->except([ 'create', 'edit']);
route::get('Cargo/getofertas/{id}','CargoController@getofertas');
route::post('Cargo/AddCategorias','CargoController@addCategorias');
route::post('Cargo/addPruebas','CargoController@addPruebas');


Route::resource('Cuestionario_encuesta', 'Cuestionario_encuestaController')->except([ 'create', 'edit']);


Route::resource('Cuestionario_psicotecnica', 'Cuestionario_psicotecnicaController')->except([ 'create', 'edit']);
Route::resource('DocsAdjunt', 'DocsAdjuntController')->except([ 'create', 'edit']);
Route::get('DocsAdjunt/DocsAspirante', 'DocsAdjuntController@DocsAspirante');
Route::get('DocsAdjunt/showAspirante/{id}', 'DocsAdjuntController@showAspirante');
Route::get('DocsAdjunt/DoscOrganizacion', 'DocsAdjuntController@DocsOrganizacion');
Route::get('DocsAdjunt/showOrganizacion/{id}', 'DocsAdjuntController@showOrganizacion');








Route::resource('Evaluacion_seleccion', 'Evaluacion_seleccionController')->except([ 'create', 'edit']);
Route::resource('Historial_oferta', 'Historial_ofertaController')->except([ 'create', 'edit']);



Route::resource('Oferta-trabajo', 'Oferta_trabajoController')->except([ 'create', 'edit']);
Route::post('Oferta-trabajo/addCargos','Oferta_trabajoController@addCargos');
route::post('Oferta-trabajo/AddCategorias','Oferta_trabajoController@addCategorias');


Route::resource('Organizacion', 'OrganizacionController')->except([ 'create', 'edit']);
Route::resource('Pregunta_imagen', 'Pregunta_imagenController')->except([ 'create', 'edit']);
Route::resource('Pregunta', 'PreguntaController')->except([ 'create', 'edit']);



Route::resource('Prueba_cuestionario', 'Prueba_cuestionarioController')->except([ 'create', 'edit']);
Route::resource('Prueba_video', 'Prueba_videoController')->except([ 'create', 'edit']);



Route::resource('Prueba', 'PruebaController')->except([ 'create', 'edit']);
Route::resource('Respuesta', 'RespuestaController')->except([ 'create', 'edit']);

Route::resource('Administrador', 'AdministradorController')->except([ 'create', 'edit']);
Route::get('Administrador/filtrar_PorOrganizacion/{id_organizacion}', 'AdministradorController@filtrar_PorOrganizacion');
Route::post('Administrador/CambiarOrganizacion', 'AdministradorController@cambiarDeOrgranizacion');

Route::resource('Reclutador', 'ReclutadorController')->except([ 'create', 'edit']);

Route::get('Reclutador/filtrar_PorOrganizacion/{id_organizacion}', 'OrganizacionReclutadorController@filtrar_PorOrganizacion');

Route::post('Reclutador/CambiarOrganizacion', 'OrganizacionReclutadorController@cambiarDeOrgranizacion');

Route::resource('SuperUser', 'SuperUserController')->except([ 'create', 'edit','update']);
Route::resource('Persona', 'PersonaController')->except([ 'create', 'edit']);