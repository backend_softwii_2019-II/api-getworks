<?php

namespace App\Http\Controllers;

use App\models\Pruebas_cuestionario;
use Illuminate\Http\Request;

class Prueba_cuestionarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($data=Pruebas_cuestionario::all()){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $prueba_cuestionario = new Pruebas_cuestionario();
        $prueba_cuestionario->prueba_id = $request->prueba_id;
        if($prueba_cuestionario->save()){
            return response()->json([
                'data'=> $prueba_cuestionario,
                'success'=>true,
                'message'=>'Successfully store processed',
                'code'=>201,
            ],201);
           
        }else{
            return response()->json([
                'estatus'=>'error',
                'code'=>'404',
                ],404);
        
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Pruebas_cuestionario::findOrFail($id);
        if(is_object($data))
        {
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $prueba_cuestionario =  Pruebas_cuestionario::findOrFail($id);
        $prueba_cuestionario->prueba_id = $request->prueba_id;
        if($prueba_cuestionario->save()){
            return response()->json([
                'data'=> $prueba_cuestionario,
                'success'=>true,
                'message'=>'Successfully store processed',
                'code'=>201,
            ],201);
           
        }else{
            return response()->json([
                'estatus'=>'error',
                'code'=>'404',
                ],404);
        
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Pruebas_cuestionario::findOrFail($id);
       
        if(is_object($data)){
           
            if($data->delete()){
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$data
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }
}
