<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreguntasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preguntas', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('pregunta')->nullable();
            $table->string('descripcion')->nullable();
            $table->integer('puntuacion')->nullable();
            //$table->unsignedInteger('imagenes_id')->nullable;
            //$table->foreign('imagenes_id')->references('id')->on('imagenes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preguntas');
    }
}
