<?php

use Illuminate\Database\Seeder;
use App\models\Categoria;


class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categoria::create([
            'nombre' => "Testing",
            'descripcion'=>""
        ]);
        Categoria::create([
            'nombre' => "Desarrollo",
            'descripcion'=>""
        ]);
        Categoria::create([
            'nombre' => "Administracion",
            'descripcion'=>""
        ]);
        Categoria::create([
            'nombre' => "Mecanica",
            'descripcion'=>""
        ]);
        Categoria::create([
            'nombre' => "Recursos Humanos",
            'descripcion'=>""
        ]);
        Categoria::create([
            'nombre' => "Atencion al Cliente",
            'descripcion'=>""
        ]);
        Categoria::create([
            'nombre' => "Compras / Comercio Exterior",
            'descripcion'=>""
        ]);
        Categoria::create([
            'nombre' => "Medicina / Salud",
            'descripcion'=>""
        ]);
        Categoria::create([
            'nombre' => "CallCenter",
            'descripcion'=>""
        ]);
        Categoria::create([
            'nombre' => "Legal",
            'descripcion'=>""
        ]);
        Categoria::create([
            'nombre' => "Ventas",
            'descripcion'=>""
        ]);
        Categoria::create([
            'nombre' => "Produccion",
            'descripcion'=>""
        ]);
        Categoria::create([
            'nombre' => "Docencia",
            'descripcion'=>""
        ]);
        Categoria::create([
            'nombre' => "Informatica / Telecomunicaciones",
            'descripcion'=>""
            
        ]);
        Categoria::create([
            'nombre' => "Ingenieria",
            'descripcion'=>""
        ]);
    }
}
