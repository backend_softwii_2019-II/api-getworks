<?php

namespace App\Http\Controllers;

use App\models\Aspirante;
use App\models\Pregunta;
use App\models\Respuesta;
use Illuminate\Http\Request;

class RespuestaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $data=Respuesta::all()->load('preguntas');
        if(is_object($data)){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'respuesta'=>'required|string',
            'descripcion'=>'required',
            'puntuacion'=>'required',
            'preguntas_id'=>'required',
            'aspirante_id'=>'required'
        ]);
        $pregunta=Pregunta::where('id',$request->preguntas_id)->first();
        $aspirante=Aspirante::where('id',$request->aspirante_id)->first();
        
        if(!$pregunta || !$aspirante )
            {
               return response()->json([
                       'success'=>false,
                       'message'=>'we can\'t find a preguntas or aspirante  whith that id.',
                       'code'=>404,
               ],404);
           }else{
          
           $respuesta=new respuesta();
           $respuesta->respuesta=$request->respuesta;
           $respuesta->descripcion=$request->descripcion;
           $respuesta->puntuacion=$request->puntuacion;
           $respuesta->preguntas_id=$request->preguntas_id;
           $respuesta->aspirante_id=$request->aspirante_id;
           if( $respuesta->save()){
            $respuesta->preguntas()->associate($pregunta);
            $respuesta->aspirante()->associate($aspirante);
               return response()->json([
                   'data'=> $respuesta,
                   'success'=>true,
                   'message'=>'Successfully store processed',
                   'code'=>201,
               ],201);
              
           }else{
               return response()->json([
                   'estatus'=>'error',
                   'code'=>'404',
                   ],404);
           }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Respuesta::findOrFail($id)->load('preguntas');
        if(is_object($data))
        {
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }

   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'respuesta'=>'required|string',
            'descripcion'=>'required',
            'puntuacion'=>'required',
            'preguntas_id'=>'required'
        ]);
        $pregunta=Pregunta::where('id',$request->preguntas_id)->first();
        if(!$pregunta)
            {
               return response()->json([
                       'success'=>false,
                       'message'=>'we can\'t find a preguntas  whith that id.',
                       'code'=>404,
               ],404);
           }else{
          
           $respuesta= respuesta::findorfail($id);
           if(is_object($respuesta)){
                $respuesta->respuesta=$request->respuesta;
                $respuesta->descripcion=$request->descripcion;
                $respuesta->puntuacion=$request->puntuacion;
                $respuesta->preguntas_id=$request->preguntas_id;
                if( $respuesta->save()){
                    $respuesta->preguntas()->associate($pregunta);
   
                    return response()->json([
                   'data'=> $respuesta,
                   'success'=>true,
                   'message'=>'Successfully store processed',
                   'code'=>201,
                    ],201);
              
                }else{
                    return response()->json([
                   'estatus'=>'error',
                   'code'=>'404',
                   ],404);
                }
            }else{
                return response()->json([
               'estatus'=>'error',
               'code'=>'404',
               ],404);
            }
          
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Respuesta::findOrFail($id);
       
        if(is_object($data)){
           
            if($data->delete()){
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$data
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }
}
