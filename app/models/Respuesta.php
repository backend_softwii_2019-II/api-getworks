<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Respuesta extends Model
{
    protected $table='respuestas';
    //atributes
    
    protected $fillable = [
        'respuesta', 'puntuacion', "descripcion","preguntas_id"
    ];
    public function preguntas(){
        return $this->belongsTo('App\models\Pregunta');
    }
    public function aspirante(){
        return $this->belongsTo('App\models\Aspirante');
    }
}
