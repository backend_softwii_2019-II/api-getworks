<?php

namespace App\Http\Controllers;

use App\models\Pregunta;
use Illuminate\Http\Request;

class PreguntaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($data=Pregunta::all()){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }

    
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'pregunta'=>'required|string',
            'descripcion'=>'required',
            'puntuacion'=>'required',
        ]);
        
           $pregunta=new Pregunta();
           $pregunta->pregunta=$request->pregunta;
           $pregunta->descripcion=$request->descripcion;
           $pregunta->puntuacion=$request->puntuacion;
           if( $pregunta->save()){
               
   
               return response()->json([
                   'data'=> $pregunta,
                   'success'=>true,
                   'message'=>'Successfully store processed',
                   'code'=>201,
               ],201);
              
           }else{
               return response()->json([
                   'estatus'=>'error',
                   'code'=>'404',
                   ],404);
           }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Pregunta::findOrFail($id);
        if(is_object($data))
        {
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }

   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'pregunta'=>'required|string',
            'descripcion'=>'required',
            'puntuacion'=>'required',
        ]);
        
           $pregunta= Pregunta::findOrFail($id);
           if(is_object($pregunta)){
                $pregunta->pregunta=$request->pregunta;
                $pregunta->descripcion=$request->descripcion;
                $pregunta->puntuacion=$request->puntuacion;
                    if( $pregunta->save()){
                
    
                         return response()->json([
                            'data'=> $pregunta,
                            'success'=>true,
                            'message'=>'Successfully store processed',
                            'code'=>201,
                             ],201);
               
                     }else{
                        return response()->json([
                            'estatus'=>'error',
                            'code'=>'404',
                            ],404);
                    }
            }else{
                     return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
                    ],404);
            }
          
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Pregunta::findOrFail($id);
       
        if(is_object($data)){
           
            if($data->delete()){
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$data
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }
}
