<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuestionariosEncuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuestionarios_encuestas', function (Blueprint $table) {
            $table->Increments('id');
            $table->unsignedInteger('pruebas_cuestionarios_id');
            $table->foreign('pruebas_cuestionarios_id')->references('id')->on('pruebas_cuestionarios')->onDelete('restrict')->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuestionarios_encuestas');
    }
}
