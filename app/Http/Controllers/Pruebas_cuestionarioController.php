<?php

namespace App\Http\Controllers;

use App\models\Prueba;
use App\models\Pruebas_cuestionario;
use Illuminate\Http\Request;

class Pruebas_cuestionarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($cuestionario=Pruebas_cuestionario::all()){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$cuestionario
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tipo_prueba'=>'required|string',
            'ponderacion'=>'required|string',
            'prueba_id'=>'required'
        ]);
        $prueba=Prueba::where('id',$request->prueba_id)->first();
         if(!$prueba)
             {
                return response()->json([
                        'success'=>false,
                        'message'=>'we can\'t find a prueba  whith that id.',
                        'code'=>404,
                ],404);
            }
        $cuestionario=new Pruebas_cuestionario();
        $cuestionario->tipo_prueba=$request->tipo_prueba;
        $cuestionario->ponderacion=$request->ponderacion;
        $cuestionario->prueba_id=$request->prueba_id; 
        if( $cuestionario->save()){
            //$cuestionario->Prueba()->associate($prueba);

            return response()->json([
                'data'=> $cuestionario,
                'success'=>true,
                'message'=>'Successfully store processed',
                'code'=>201,
            ],201);
           
        }else{
            return response()->json([
                'estatus'=>'error',
                'code'=>'404',
                ],404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cuestionario=Pruebas_cuestionario::findOrFail($id);
       
        if(is_object($cuestionario)){
            return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$cuestionario
            ],201);
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tipo_prueba'=>'required|string',
            'ponderacion'=>'required|string'
        ]);
       
        $cuestionario=Pruebas_cuestionario::findOrFail($id);
        $cuestionario->tipo_prueba=$request->tipo_prueba;
        $cuestionario->ponderacion=$request->ponderacion;

        if( $cuestionario->save()){
         return response()->json([
                'data'=> $cuestionario,
                'success'=>true,
                'message'=>'Successfully update processed',
                'code'=>201,
            ],201);
           
        }else{
            return response()->json([
                'estatus'=>'error',
                'code'=>'404',
                ],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cuestionario=Pruebas_cuestionario::findOrFail($id);
       
        if(is_object($cuestionario)){
           
            if($cuestionario->delete()){
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$cuestionario
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }
}
