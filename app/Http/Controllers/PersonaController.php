<?php

namespace App\Http\Controllers;

use App\models\Persona;
use App\models\User;
use Illuminate\Http\Request;

class PersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        if($data=Persona::all()->load('user')){
            return response()->json([
            'status'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else{
            return response()->json([
            'status'=>'error',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'apellido'=>'required|string',
            'fechadenacimiento'=>'required',
            'nrocelular'=>'required',
            'user_id'=>'required'
        ]);
        $user=User::where('id',$request->user_id)->first();
        if(!$user)
            {
               return response()->json([
                       'success'=>false,
                       'message'=>'we can\'t find a user whith that id.',
                       'code'=>404,
               ],404);
           }
          
           $persona=new Persona();
           $persona->apellido=$request->apellido;
           $persona->nrocelular=$request->nrocelular;
           $persona->fechadenacimiento=$request->fechadenacimiento;
           $persona->user_id=$request->user_id; 
           if( $persona->save()){
               $persona->user()->associate($user);
   
               return response()->json([
                   'data'=> $persona,
                   'success'=>true,
                   'message'=>'Successfully store processed',
                   'code'=>201,
               ],201);
              
           }else{
               return response()->json([
                   'status'=>'error',
                   'code'=>'404',
                   ],404);
           }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Persona::findOrFail($id)->load('user');
        if(is_object($data))
        {
            return response()->json([
            'status'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else
       {
            return response()->json([
            'status'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'apellido'=>'required|string',
            'fechadenacimiento'=>'required',
            'nrocelular'=>'required',
            'user_id'=>'required'
        ]);
        
        $user=User::where('id',$request->user_id)->first();
        if(!$user)
            {
               return response()->json([
                       'success'=>false,
                       'message'=>'we can\'t find a user whith that id.',
                       'code'=>404,
               ],404);
           }
          
           $persona=Persona::findOrFail($id);

           $persona->apellido=$request->apellido;
           $persona->nrocelular=$request->nrocelular;
           $persona->fechadenacimiento=$request->fechadenacimiento;
           $persona->user_id=$request->user_id; 
           if( $persona->save()){
               $persona->user()->associate($user);
   
               return response()->json([
                   'data'=> $persona,
                   'success'=>true,
                   'message'=>'Successfully store processed',
                   'code'=>201,
               ],201);
              
           }else{
               return response()->json([
                   'status'=>'error',
                   'code'=>'404',
                   ],404);
           }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Persona::findOrFail($id);
       
        if(is_object($data)){
           
            if($data->delete()){
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$data
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }
}
