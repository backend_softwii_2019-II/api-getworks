<?php

namespace App\Http\Controllers;

use App\models\Prueba;
use Illuminate\Http\Request;

class CuestionarioPreguntaController extends Controller
{
    public function store(Request $request)
    {
        $prueba = Prueba::find($request->cuestionario_id);
        if($prueba->tipo_prueba == "psicotecnica")
        {
            $imagenes = $request->file("imagenes");
        }
        $data_imagenes = array();
        $preguntas = array();
        foreach ($request->preguntas as $key => $value) {
            $pregunta = new Pregunta();
            $pregunta->pregunta = $value;
            $pregunta->descripcion = $descripcion;
            $pregunta->puntuacion = $puntuacion;
            $pregunta->save();

            if($prueba->tipo_prueba == "psicotecnica"){

                //ruta de la carpeta
                $carpeta = "public/preguntas_imagenes/Prueba-" . $prueba->id;
                

                //Esto lo hago para crear la carpeta
                if (!file_exists($carpeta))
                {
                    mkdir($carpeta, 0755, true);
                }

                if(is_array($imagenes))
                {
                    $nombre = "imagen_" . date("Y_m_d_H_i_s") . "_" . $key . "." . $imagen->getClientOriginalExtension();
                    $foto->move($carpeta, $nombre);
                    array_push($data_imagenes, array("pregunta_id" => $pregunta->id, "ruta" => $carpeta . "/" . $nombre,"formato" => $imagen->getClientOriginalExtension(), "created_at" => date("Y-m-d H:i:s"), "updated_at" => date("Y-m-d H:i:s")));
                   
                }
                
            }

            array_push($preguntas, array("pruebas_cuestionarios_id" => $request->cuestionario_id, "preguntas_id" => $pregunta->id));
           
        }
        if(!empty($data_imagenes)){
            Pregunta_imagen::insert($data_imagenes);
        }
        if (!empty($preguntas))
        {
            if(CuestionarioPregunta::insert($preguntas))
            {
                return response()->json([
                    'data'=> $preguntas,
                    'success'=>true,
                    'message'=>'Successfully store processed',
                    'code'=>201,
                ],201);
               
            }else{
                return response()->json([
                    'estatus'=>'error',
                    'code'=>'404',
                    ],404);
            
            }
        }        
    }
}
