<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfertaTrabajosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oferta_trabajos', function (Blueprint $table) {
            $table->Increments('id');
          
            $table->string('nombre_oferta')->nullable();
            $table->string('descripcion')->nullable();
            $table->date('fechaApertura')->nullable();
            $table->date('fechaCierre')->nullable();
            $table->string('ubicacion')->nullable();
            $table->unsignedInteger('reclutadores_id');
            $table->foreign('reclutadores_id')->references('id')->on('reclutadores')->onDelete('restrict')->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oferta_trabajos');
    }
}
