<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuestionariosPreguntasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuestionarios_preguntas', function (Blueprint $table) {
            $table->Increments('id');
            $table->unsignedInteger('preguntas_id')->nullable();
            $table->foreign('preguntas_id')->references('id')->on('preguntas')->onDelete('cascade');
            $table->unsignedInteger('pruebas_cuestionarios_id')->nullable();
            $table->foreign('pruebas_cuestionarios_id')->references('id')->on('pruebas_cuestionarios')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuestionarios_preguntas');
    }
}
