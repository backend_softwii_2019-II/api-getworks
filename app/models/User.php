<?php

namespace App\models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table='users';
    //atributes
    
    protected $fillable = [
        'nombre', 'email', 'password',"nrodocumentoidentidad","nrofijo","descripcion","direccion",
        "nickname","rol","imagen"
    ];

   
    protected $hidden = [
        'password', 'remember_token',
    ];
    //Relaciones de muchos a muchos
    public function organizaciones()
    {
        return $this->hasOne('App\models\Organizacion');
    }
    //relaciones 1 a muchos
    public function persona(){
        return $this->belongsToMany('App\models\Persona');
    }
    
    
}
