<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Cuestionario extends Model
{
    //relaciones de muchos amuchos
    public function cuestionario_preguntas()
    {
        return $this->hasMany('App\models\CuestionarioPregunta');
    }
    //relaciones de muchos amuchos
    public function cuestionario_encuesta()
    {
        return $this->belongsToMany('App\models\Cuestionario_encuesta');
    }
    //relaciones de muchos amuchos
    public function cuestionario_psicotecnica()
    {
        return $this->belongsToMany('App\models\Cuestionario_psicotecnica');
    }
}
