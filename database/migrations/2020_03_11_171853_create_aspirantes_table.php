<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAspirantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aspirantes', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('especialidad')->nullable();
            $table->string('nivel_academico')->comment('nivel academico')->nullable();
            $table->string('nivel_experiencia')->comment('nivel de experiencia')->nullable();
            $table->unsignedInteger('persona_id');
            $table->foreign('persona_id')->references('id')->on('personas')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedInteger('docs_adjunt_id')->nullable();
            $table->foreign('docs_adjunt_id')->references('id')->on('docs_adjunts')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aspirantes');
    }
}
