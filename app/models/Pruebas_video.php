<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Pruebas_video extends Model
{
    protected $table='pruebas_videos';
    //atributes
    
    protected $fillable = [
        'nombreVideo', 'formato',"rutaVideo","video", "descripcion"
    ];
    public function prueba()
    {
        return $this->belongsTo('App\models\Prueba');
    }
}
