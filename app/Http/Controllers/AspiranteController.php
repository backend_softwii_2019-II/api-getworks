<?php

namespace App\Http\Controllers;

use App\models\Aspirante;
use App\models\Organizacion;
use App\models\User;
use App\models\Persona;
use Illuminate\Http\Request;

class AspiranteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Aspirante::with('persona.user')->get();
        if($data){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }

   
    public function store(Request $request)
    {
        $request->validate([
            'especialidad'=>'required|string',
            'nivel_academico'=>'required|string',
            'nivel_experiencia'=>'required|string',
            'persona_id'=>'required'
        ]);
        $persona=Persona::where('id',$request->persona_id)->first();
         if(!$persona)
             {
                return response()->json([
                        'success'=>false,
                        'message'=>'we can\'t find a persona whith that id.',
                        'code'=>404,
                ],404);
            }

       
        $aspirante=new Aspirante();
        $aspirante->especialidad=$request->especialidad;
        $aspirante->nivel_academico=$request->nivel_academico;
        $aspirante->nivel_experiencia=$request->nivel_experiencia;
        $aspirante->persona_id=$persona->id; 

        if($aspirante->save()){
            
            $aspirante->persona()->associate($persona);

            return response()->json([
                'data'=> $aspirante,
                'success'=>true,
                'message'=>'Successfully store processed',
                'code'=>201,
            ],201);
           
        }else{
            return response()->json([
                'estatus'=>'error',
                'code'=>'404',
                ],404);
        }
    }

    public function show($id)
    {
        $aspirante=Aspirante::with('persona.user')->where('aspirantes.id','=',$id)->first();
       
        if(is_object($aspirante)){
            return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$aspirante
            ],201);
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }

   
   

    
    public function update(Request $request, $id)
    {
        $request->validate([
            'especialidad'=>'required|string',
            'nivel_academico'=>'required|string',
            'nivel_experiencia'=>'required|string',
        ]);
       
        $aspirante= Aspirante::where('aspirantes.id',$id)->with('persona.user')->first();

        $aspirante->especialidad=$request->especialidad;
        $aspirante->nivel_academico=$request->nivel_academico;
        $aspirante->nivel_experiencia=$request->nivel_experiencia;
        $aspirante->persona->apellido=$request->apellido;
        $aspirante->persona->fechadenacimiento = $request->fechadenacimiento;
        $aspirante->persona->nrocelular = $request->nrocelular;
        $aspirante->persona->user->nombre=$request->nombre;
        $aspirante->persona->user->nrodocumentoidentidad=$request->cedula;
        $aspirante->persona->user->nrofijo=$request->nrofijo;
        $aspirante->persona->user->descripcion=$request->descripcion;
        $aspirante->persona->user->direccion=$request->direccion;
        $aspirante->persona->user->nickname=$request->nickname;
        $aspirante->persona->user->email=$request->email;

        if( $aspirante->save()){
         return response()->json([
                'data'=> $aspirante,
                'success'=>true,
                'message'=>'Successfully update processed',
                'code'=>201,
            ],201);
           
        }else{
            return response()->json([
                'estatus'=>'error',
                'code'=>'404',
                ],404);
        }
    }

     
    public function destroy($id)
    {
        $aspirante=Aspirante::findOrFail($id);
        $persona=Persona::findOrFail($aspirante->persona_id);
        $user=User::findOrFail($persona->user_id);
       
        if(is_object($aspirante) and is_object($persona) and is_object($user)){
           
            if($aspirante->delete() and $persona->delete() and $user->delete()){
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'message'=>"Cuenta eliminada con exito"
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }
}
