<?php

namespace App\Http\Controllers;

use App\models\Aspirante;
use App\models\Historial_academico;
use Illuminate\Http\Request;

class NivelAcademicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $academico=Historial_academico::all();
        if(is_object($academico)){
            return response()->json([
                'status'=>'success',
                'code'=>'200',
                'data'=>$academico
            ],200);
        }else
        {
            return response()->json([
                'status'=>'error',
                'code'=>'404',
                'message'=>'resource not found'
            ],404);
        }
    }

   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nivel_academico'=>'required|string',
            'institucion'=>'required',
            'fecha_registro'=>'required',
            'aspirantes_id'=>'required'
        ]);
        $aspirante=Aspirante::where('id',$request->aspirantes_id)->first();
        if(!$aspirante)
            {
               return response()->json([
                       'success'=>false,
                       'message'=>'we can\'t find a aspirante  whith that id.',
                       'code'=>404,
               ],404);
           }
          
           $academico=new Historial_academico();
           $academico->institucion=$request->institucion;
           $academico->nivel_academico=$request->nivel_academico;
           $academico->fecha_registro=$request->fecha_registro;
           $academico->descripcion=$request->descripcion;
           $academico->aspirantes_id=$request->aspirantes_id; 
           if( $academico->save()){
               $academico->aspirante()->associate($aspirante);
   
               return response()->json([
                   'data'=> $academico,
                   'success'=>true,
                   'message'=>'Successfully store processed',
                   'code'=>201,
               ],201);
              
           }else{
               return response()->json([
                   'estatus'=>'error',
                   'code'=>'404',
                   ],404);
           }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $academico=Historial_academico::findOrFail($id)->load("aspirante");
       
        if(is_object($academico)){
           return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$academico
                    ],201);
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }

    
   
    public function update(Request $request, $id)
    {
        $request->validate([
            'nivel_academico'=>'required|string',
            'institucion'=>'required',
            'fecha_registro'=>'required',
        ]);
       
           $academico=Historial_academico::findorfail($id);
           $academico->institucion=$request->institucion;
           $academico->nivel_academico=$request->nivel_academico;
           $academico->fecha_registro=$request->fecha_registro;
           $academico->descripcion=$request->descripcion;

           if( $academico->save()){
               return response()->json([
                   'data'=> $academico,
                   'success'=>true,
                   'message'=>'Successfully store processed',
                   'code'=>201,
               ],201);
              
           }else{
               return response()->json([
                   'estatus'=>'error',
                   'code'=>'404',
                   ],404);
           }
    }

    
    public function destroy($id)
    {
        $academico=Historial_academico::findOrFail($id);
       
        if(is_object($academico)){
           
            if($academico->delete()){
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$academico
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }
}
