<?php

namespace App\Http\Controllers;

use App\models\Cuestionario_encuesta;
use Illuminate\Http\Request;

class Cuestionario_encuestaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($data=Cuestionario_encuesta::all()){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function store(Request $request)
    {
        $data = new Cuestionario_encuesta();
        $data->pruebas_cuestionario_id = $request->pruebas_cuestionario_id;
        if($data->save()){
            return response()->json([
                'data'=> $data,
                'success'=>true,
                'message'=>'Successfully store processed',
                'code'=>201,
            ],201);
           
        }else{
            return response()->json([
                'estatus'=>'error',
                'code'=>'404',
                ],404);
        
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Cuestionario_encuesta::findOrFail($id);
        if(is_object($data))
        {
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data =  Cuestionario_encuesta::findOrFail($id);
        $data->prueba_id = $request->prueba_id;
        if($data->save()){
            return response()->json([
                'data'=> $data,
                'success'=>true,
                'message'=>'Successfully store processed',
                'code'=>201,
            ],201);
           
        }else{
            return response()->json([
                'estatus'=>'error',
                'code'=>'404',
                ],404);
        
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Cuestionario_encuesta::findOrFail($id);
       
        if(is_object($data)){
           
            if($data->delete()){
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$data
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }
}
