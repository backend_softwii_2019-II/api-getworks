<?php

namespace App\Http\Controllers;

use App\models\Organizacion;
use App\models\Aspirante;
use App\models\Reclutador;
use App\models\Persona;
use App\models\User;
use Illuminate\Http\Request;

class OrganizacionReclutadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Reclutador::with('persona.user')->get();
        if($data){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id'=>'required',
            'organizacion_id'=>'required',
            'apellido'=>'required|string',
            'fechadenacimiento'=>'required|string',
            'nrocelular'=>'required|string',
        ]);

        $organizacion= Organizacion::findOrFail($request->organizacion_id);
        $user = User::findOrFail($request->user_id);

        if(!$organizacion)
             {
                return response()->json([
                        'success'=>false,
                        'message'=>'No existe una empresa con ese id',
                        'code'=>404,
                ],404);
            }
        $usuarioOrganizacion = Organizacion::where("user_id","=",$user->id)->first();
        if(is_object($usuarioOrganizacion))
             {
                return response()->json([
                        'success'=>false,
                        'message'=>'Este usuario es una organizacion',
                        'code'=>404,
                ],404);
            }
        if(!$user)
             {
                return response()->json([
                        'success'=>false,
                        'message'=>'No existe un usuario con ese id',
                        'code'=>404,
                ],404);
            }
        
        $personaCreada = Persona::where("user_id","=",$user->id)->first();
        if(is_object($personaCreada))
        {   
            if (Reclutador::where("persona_id","=",$personaCreada->id)) {
                return response()->json([
                        'success'=>false,
                        'message'=>'Fallo al crear por que esta persona ya era un Reclutador',
                        'code'=>404,
                ],404);
            }
            $persona = $personaCreada;
        }else{
            $persona=new Persona();
            $persona->apellido = $request->apellido;
            $persona->fechadenacimiento = $request->fechadenacimiento;
            $persona->nrocelular = $request->nrocelular;
            $persona->user_id = $user->id;

            if(!$persona->save())
             {
                return response()->json([
                        'success'=>false,
                        'message'=>'Fallo por datos personales incorrectos',
                        'code'=>404,
                ],404);
            }
        }

        $reclutador = new Reclutador();
        $reclutador->organizacion_id=$organizacion->id;
        $reclutador->persona_id=$persona->id;

        if($reclutador->save()){
            $user->rol = "Reclutador";
            $user->save();
            $persona->user()->associate($user);
            $reclutador->persona()->associate($persona);
            $reclutador->organizacion()->associate($organizacion);


            return response()->json([
                'success'=>true,
                'data'=>$reclutador,
                'message'=>'Successfully store processed',
                'code'=>201,
            ],201);
           
        }else{
            return response()->json([
                'estatus'=>'error',
                'code'=>'404',
                ],404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrganizacionReclutador  $organizacionReclutador
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reclutador=Reclutador::with('persona.user')->where('id','=',$id)->first();
       
        if(is_object($reclutador)){
            return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$reclutador
            ],201);
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrganizacionReclutador  $organizacionReclutador
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            'user_id'=>'required',
            'apellido'=>'required|string',
            'fechadenacimiento'=>'required|string',
            'nrocelular'=>'required|string',
        ]);
       
        $reclutador= Reclutador::where('aspirantes.id',$id)->with('persona.user')->first();


        $reclutador->persona->apellido=$request->apellido;
        $reclutador->persona->fechadenacimiento = $request->fechadenacimiento;
        $reclutador->persona->nrocelular = $request->nrocelular;
        $reclutador->persona->user->nombre=$request->nombre;
        $reclutador->persona->user->nrodocumentoidentidad=$request->cedula;
        $reclutador->persona->user->nrofijo=$request->nrofijo;
        $reclutador->persona->user->descripcion=$request->descripcion;
        $reclutador->persona->user->direccion=$request->direccion;
        $reclutador->persona->user->nickname=$request->nickname;
        $reclutador->persona->user->email=$request->email;

        if( $reclutador->save()){
         return response()->json([
                'data'=> $reclutador,
                'success'=>true,
                'message'=>'Successfully update processed',
                'code'=>201,
            ],201);
           
        }else{
            return response()->json([
                'estatus'=>'error',
                'code'=>'404',
                ],404);
        }
    }

    public function filtrar_PorOrganizacion($id_organizacion)
    {
        $data = Reclutador::with(['persona.user','organizacion'])->where('organizacion_id','=',$id_organizacion)->get();
        if(is_object($data))
        {
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }

    public function cambiarDeOrgranizacion(Request $request){
        $reclutador = Reclutador::findOrFail($request->id_reclutador);
        $organizacionOld = Organizacion::findOrFail($request->id_organizacionOld);
        $organizacionNew = Organizacion::findOrFail($request->id_organizacionNew);

        if(is_object($organizacionNew) and is_object($reclutador) and is_object($organizacionOld)){
            $reclutador->organizacion_id = $organizacionNew->id;
            if ($reclutador->save()) {
                $reclutador->organizacion()->associate($organizacionNew);
                return response()->json([
                'success'=>true,
                'data'=>$reclutador,
                'message'=>'Successfully store processed',
                'code'=>201,]);
            }else{
                return response()->json([
                'estatus'=>'error',
                'message'=>'Fallo al guardar de nuestro lado, intente de nuevo',
                'code'=>'404',
                ],404);
            }
        }else{
            return response()->json([
            'estatus'=>'error',
            'message'=>'Fallo de datos, Revise los datos y vuelva a intentarlo',
            'code'=>'404',
            ],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrganizacionReclutador  $organizacionReclutador
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reclutador = Reclutador::with(['persona.user','organizacion'])->where('id','=',$id)->first();
       
        if(is_object($reclutador)){
            if($reclutador->delete()){
                $aspirante = new Aspirante();
                $reclutador->persona->user->rol="Aspirante";
                $reclutador->persona->user->save();
                $aspirante->persona_id = $reclutador->persona_id;
                $aspirante->save();
                return response()->json([
                    'code'=>'200',
                    'status'=>'success'
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }
}
