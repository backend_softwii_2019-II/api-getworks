<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Apuntado_evaluado extends Model
{
    protected $table='Apuntado_evaluados';
    //atributes
    
    protected $fillable = [
        'fase', 'fecha_evaluacion', "desempeno","puntaje","selecto"
    ];

    //relaciones inversas 1 a m
    public function cargo_prueba(){
        return $this->belongsTo('App\models\Cargo_prueba');
    }
    public function Aspirante(){
        return $this->belongsTo('App\models\Aspirante');
    }


    //relaciones 1 a m 
    public function evaluacion_selecciones(){
        return $this->hasMany('App\models\Evaluacion_seleccion');
    }
}
