<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePruebasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pruebas', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('tipo_prueba')->nullable();
            $table->string('descripcion')->nullable();
            $table->string('puntuacion')->nullable();
           // $table->unsignedInteger('videos_id')->nullable;
            //$table->foreign('videos_id')->references('id')->on('videos')->onDelete('cascade');
            //$table->unsignedInteger('cuestionarios_id')->nullable;
            //$table->foreign('cuestionarios_id')->references('id')->on('cuestionarios')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pruebas');
        
    }
}
