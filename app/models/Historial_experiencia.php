<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Historial_experiencia extends Model
{
    protected $table='historial_experiencias';
    //atributes
    
    protected $fillable = [
        'cargo', 'nombre_organizacion', 'duracion_empleo',"fecha_registro","descripcion",'id_aspirantes'
    ];

    //relacion inversa 
    public function aspirante()
    {
        return $this->belongsTo('App\models\Aspirante');
    }
    

}
