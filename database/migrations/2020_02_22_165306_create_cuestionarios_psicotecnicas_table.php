<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuestionariosPsicotecnicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuestionarios_psicotecnicas', function (Blueprint $table) {
            $table->Increments('id');
            $table->unsignedInteger('prueba_id');
            $table->foreign('prueba_id')->references('id')->on('pruebas')->onDelete('restrict')->onUpdate('restrict');
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuestionarios_psicotecnicas');
    }
}
