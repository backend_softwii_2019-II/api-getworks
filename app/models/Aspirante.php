<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Aspirante extends Model
{
    protected $table='aspirantes';
    //atributes
    
    protected $fillable = [
        'especialidad ', 'nivel_academico', 'nivel_experiencia','persona_id'
    ];



    //relaciones de uno a muchos
    public function historial_academicos()
    {
        return $this->hasMany('App\models\Historial_academicos');
    }
    public function historial_experiencias()
    {
        return $this->hasMany('App\models\Historial_experiencias');
    }
    public function docs_adjunts()
    {
        return $this->hasMany('App\models\docs_adjunts');
    }
    public function apuntados_evaluados()
    {
        return $this->hasMany('App\models\Apuntado_evaluado');
    }
    public function respuestas()
    {
        return $this->hasMany('App\models\Respuesta');
    }
    //relaciones inversas
    public function persona(){
        return $this->belongsTo('App\models\Persona');
    }
}
