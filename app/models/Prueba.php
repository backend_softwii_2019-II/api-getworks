<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Prueba extends Model
{
    protected $table='pruebas';
    //atributes
    
    protected $fillable = [
        'tipo_prueba',"descripcion"
    ];

    //relaciones de 1 a 1
    public function pruebas_cuestionario(){
        return $this->blongsTo('App\models\Prueba_cuestionario');
    }
    public function cuestionario_psicotecnica(){
        return $this->blongsTo('App\models\Cuestionario_psicotecnica');
    }

    public function pruebas_videos(){
        return $this->blongsTo('App\models\Prueba_video');
    }
    //relaciones de 1 muchos
    public function cargos(){
        return $this->belongsToMany('App\models\Cargo');
    }
}
