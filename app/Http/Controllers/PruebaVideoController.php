<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Prueba;
use App\models\Pruebas_video;

class PruebaVideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       if($categoria=Pruebas_video::all()){
            return response()->json([
            'status'=>'success',
            'code'=>'200',
            'data'=>$categoria
        
            ],200);
       } else{
            return response()->json([
            'status'=>'error',
            'code'=>'404',
            ],404);
       }
        
    }

    
    
    public function store(Request $request)
    {
        $request->validate([
            'descripcion'=>'required     '
            
            ]);
            $data=$request->all();
        $categoria=Categoria::create($data);
        if(is_object($categoria)){
            return response()->json([
                'status'=>'success',
                'code'=>201,
                'data'=> $categoria
            ],201);

        }else{
            return response()->json([
                'status'=>'error',
                'code'=>404,
                'message'=>''
            ],200);
        }
        
    }

   
    public function show($id)
    {   
        $categoria=Categoria::findOrFail($id);
        if(is_object($categoria))
        {
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$categoria
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }

    
 
   
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|string|unique:categorias',
            'descripcion'=>'required'
        ]);
        //realizamos la actualizacion del los datos
        $category=Categoria::findorFail($id);
        if(is_object($category)){
            $category->name=$request->name;
           if($category->save()){
                return  response()->json([
                    'data'=>$category,
                    'status'=>'success',
                    'code'=>'200'
            
                ],200);
            }else{
                return  response()->json([
                    'message'=>'error al actualizar',
                    'status'=>'error',
                    'code'=>'404'
                
                ],200);  
            }
        
        }else{
            return response()->json([
                'status'=>'error',
                'message'=>'la entrada no existe',
                'code'=>'404'
            ],404);
        }
    }

    
    public function destroy($id)
    {
        $categoria=Categoria::findOrFail($id);
       
        if(is_object($categoria)){
           
            if($categoria->delete()){
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$categoria
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }
}
