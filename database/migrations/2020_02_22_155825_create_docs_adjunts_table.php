<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocsAdjuntsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('docs_adjunts', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('nombre')->nullable();
            $table->String('tipoDocumento')->nullable();
            $table->string('formato')->nullable();
            $table->string('rutaDocumento')->nullable();
            $table->string('descripcion')->nullable();

           
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('docs_adjunts');
    }
}
