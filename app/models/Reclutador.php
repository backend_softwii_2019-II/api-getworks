<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Reclutador extends Model
{
    //atributes
    protected $table='reclutadores';
    
    protected $fillable = [
        'user_id', 'organizacion_id'
    ];
    

    public function organizacion(){
        return $this->belongsTo('App\models\Organizacion');
    }
    public function user(){
        return $this->belongsTo('App\models\User');
    }

    //relaciones de uno a muchos
    public function oferta_trabajos(){
        return $this->hasMany('App\models\Oferta_trabajo','reclutadores_id');
    }
}
