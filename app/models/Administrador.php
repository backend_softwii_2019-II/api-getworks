<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Administrador extends Model
{
    //
    protected $table='administradores';
    //atributes
    
    protected $fillable = [
        'persona_id','organizacion_id'
    ];
    public function persona(){
        return $this->belongsTo('App\models\Persona');
    }

    //relaciones de uno a muchos
    public function organizacion(){
        return $this->belongsTo('App\models\Organizacion');
    }

}
