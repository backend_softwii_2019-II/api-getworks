<?php

namespace App\Http\Controllers;

use App\models\Cargo;
use Illuminate\Http\Request;

class CargoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($data=Cargo::all()->load('categorias','pruebas','oferta_trabajos')){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre'=>'required|string',
            'nivel_experiencia'=>'required',
            'tipo_empleo'=>'required|in:completo,parcial',
            'pago_aproximado'=>'required',
            'modalidad'=>'required|in:presencial,telework,freelance',
            'descripcion'=>'required',
           
        ]);
        
           $cargo=new Cargo();
           $cargo->nombre=$request->nombre;
           $cargo->descripcion=$request->descripcion;
           $cargo->nivel_experiencia=$request->nivel_experiencia;
           $cargo->tipo_empleo=$request->tipo_empleo;
           $cargo->modalidad=$request->modalidad;
           $cargo->pago_aproximado=$request->pago_aproximado;
           if( $cargo->save()){
               
               return response()->json([
                   'data'=> $cargo,
                   'success'=>true,
                   'message'=>'Successfully store processed',
                   'code'=>201,
               ],201);
              
           }else{
               return response()->json([
                   'estatus'=>'error',
                   'code'=>'404',
                   ],404);
           }
        
    }
    public function addCategorias(Request $request){
        
        $request->validate([
            'cargo_id'=>'required',
            'categorias_id'=>'required'
        ]);
        
        $cargo=Cargo::findOrFail($request->cargo_id);
        if(is_object($cargo))
        {
            $cargo->categorias()->attach($request->categorias_id);
            return response()->json([
            'status'=>'success',
            'code'=>'200',
            'message'=>'categoria agrega correctamente'
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }

    }
  
    
    public function show($id)
    {
        $data=Cargo::findOrFail($id)->load('categorias','pruebas','oferta_trabajos');
        if(is_object($data))
        {
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre'=>'required|string',
            'nivel_experiencia'=>'required',
            'tipo_empleo'=>'required|in:completo,parcial',
            'pago_aproximado'=>'required',
            'modalidad'=>'required|in:presencial,telework,freelance',
            'descripcion'=>'required'
        ]);
        
           $cargo= Cargo::findOrFail($id);
            if(is_object($cargo)){
                $cargo->nombre=$request->nombre;
                $cargo->descripcion=$request->descripcion;
                $cargo->nivel_experiencia=$request->nivel_experiencia;
                $cargo->tipo_empleo=$request->tipo_empleo;
                $cargo->modalidad=$request->modalidad;
                $cargo->pago_aproximado=$request->pago_aproximado;
                if( $cargo->save()){
        
                    return response()->json([
                        'data'=> $cargo,
                        'success'=>true,
                        'message'=>'Successfully store processed',
                        'code'=>201,
                    ],201);
                   
                }else{
                    return response()->json([
                        'estatus'=>'error',
                        'code'=>'404',
                        ],404);
                }
            }else{
                return response()->json([
                    'estatus'=>'error',
                    'code'=>'404',
                    ],404);
            }
           
    }

    public function addPruebas(Request $request){
        
        $request->validate([
            'cargo_id'=>'required',
            'pruebas_id'=>'required'
        ]);
        
        $cargo=Cargo::findOrFail($request->cargo_id);
        if(is_object($cargo))
        {
            $cargo->pruebas()->attach($request->pruebas_id);
            return response()->json([
            'status'=>'success',
            'code'=>'200',
            'message'=>'preuebas anexadas correctamente'
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }

    }
    public function destroy($id)
    {
        $data=Cargo::findOrFail($id);
       
        if(is_object($data)){
           
            if($data->delete()){
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$data
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }
}
