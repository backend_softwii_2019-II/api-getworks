<?php

namespace App\Http\Controllers;

use App\OrganizacionAdministrador;
use Illuminate\Http\Request;

class OrganizacionAdministradorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrganizacionAdministrador  $organizacionAdministrador
     * @return \Illuminate\Http\Response
     */
    public function show(OrganizacionAdministrador $organizacionAdministrador)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrganizacionAdministrador  $organizacionAdministrador
     * @return \Illuminate\Http\Response
     */
    public function edit(OrganizacionAdministrador $organizacionAdministrador)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrganizacionAdministrador  $organizacionAdministrador
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrganizacionAdministrador $organizacionAdministrador)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrganizacionAdministrador  $organizacionAdministrador
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrganizacionAdministrador $organizacionAdministrador)
    {
        //
    }
}
