<?php

namespace App\Http\Controllers;

use App\models\Apuntado_evaluado;
use Illuminate\Http\Request;
use Carbon\Carbon;

class Apuntado_evaluadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($data=Apuntado_evaluado::all()){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Apuntado_evaluado::findOrFail($id);
        if(is_object($data))
        {
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }

   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Apuntado_evaluado::findOrFail($id);
       
        if(is_object($data)){
           
            if($data->delete()){
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$data
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }

    //FUNCION DE UN ASPIRANTE
    public function presentar_prueba(Request $request)
    {
        //falta el validate
        //
        
        $aspirante_evaluado = new Apuntado_evaluado();
        $aspirante_evaluado->fecha_evaluacion = Carbon::now();
        $aspirante_evaluado->aspirante_id = Auth::user()->id;
        
        if($request->tipo_prueba == "video")
        {
            $video = $request->file("video");
            $prueba = new Prueba();
            $prueba->descripcion = $request->descripcion;
            $prueba->tipo_prueba = $request->tipo_prueba;
            $prueba->save();
            $prueba_video = new Pruebas_video();
            
            //ruta de la carpeta
            $carpeta = "public/videos_aspirantes/Asp-" . Auth::user()->id . "/Cargo-" . $request->cargo_id;
            

            //Esto lo hago para crear la carpeta
            if (!file_exists($carpeta))
            {
                mkdir($carpeta, 0755, true);
            }
            $nombre = "video_" . date("Y_m_d_H_i_s") . "_" . Auth::user()->nickname . "." . $video->getClientOriginalExtension();
            if($video->getClientOriginalExtension()!= "mp4" && $video->getClientOriginalExtension()!= "flv" && 
                $video->getClientOriginalExtension()!= "mkv" && $video->getClientOriginalExtension()!= "avi"
                || $video->getClientOriginalExtension()!= "mov" )
            {
                //Eliminar archivo y devolver mensaje de error
            }
            $video->move($carpeta, $nombre);

            $prueba_video->prueba_id = $prueba->id;
            $prueba_video->nombre = $request->nombre;
            $prueba_video->ruta = $carpeta . "/" . $nombre;
            
            $aspirante_evaluado->cargo_prueba_id = $prueba_video->id;


            if($prueba_video->save())
            {
                return response()->json([
                    'status'=>'success',
                    'code'=>201,
                    'data'=> $prueba_video
                ],201);
            }
            else
            {
                $prueba->delete();
                //borrar video
                return response()->json([
                    'status'=>'error',
                    'code'=>404,
                    'message'=>''
                ],200);
            }

            $aspirante_evaluado->save();

        }
        
        else
        {
            $aspirante_evaluado->cargo_prueba_id = $request->cargo_prueba_id;
            $preguntas = $request->preguntas;

            foreach ($preguntas as $key => $pregunta) {
                $respuesta = new Respuesta();
                $respuesta->pregunta_id = $pregunta;
                $respuesta->respuesta = $request->respuestas[$key];
                $respuesta->aspirante_id = Auth::user()->id;

                $respuesta->save();
            }
            if($aspirante_evaluado->save())
            {
                return response()->json([
                    'status'=>'success',
                    'code'=>201,
                    'data'=> $prueba_video
                ],201);
            }
            else
            {
                $prueba->delete();
                //borrar video
                return response()->json([
                    'status'=>'error',
                    'code'=>404,
                    'message'=>''
                ],200);
            }
        }

    }

    //Funciona para ver quienes estan aspirando | Esto es para que carguen todos los aspirantes que estan participando en la oferta
    public function cargar_aspirantes($cargo_id)
    {

        $aspirantes_pruebas=[];//{aspirante_id (o aspirante), {apuntado_evaluado1,apuntado_evaluado1,apuntado_evaluado1} 
                            //Donde apuntado_evaluado son las disntintas pruebas realizadas por los
                            //distintos cargo_pruebas
        //Aqui buscamos los aspirantes que participan en la oferta del $cargo_id
        $aspirantes = DB::table('users')
        ->join('personas', 'users.id', '=', 'personas.user_id')
        ->join('aspirantes', 'personas.id', '=', 'aspirantes.persona_id')
        ->join('apuntado_evaluados', 'aspirantes.id', '=', 'apuntado_evaluados.aspirante_id')
        ->join('cargo_pruebas', 'cargos.id', '=', 'cargo_pruebas.cargo_id')
        ->where('cargo_pruebas.cargo_id', '=', $cargo_id)
        ->select('users.id', 'users.nickname', 'users.nombre', 'personas.apellido', 'users.email')
        ->groupBy('users.id')
        ->get();

        for ($i=0; $i < sizeof($aspirantes); $i++) { 
            //Tras encontrar a los aspirantes, buscamos las evaluaciones que hayan hecho para la prueba
            $apuntado_evaluados = DB::table('apuntado_evaluados')
            ->join('cargo_pruebas', 'cargo_pruebas.id', '=', 'apuntado_evaluados.cargo_prueba_id')
            ->join('cargos', 'cargos.id', '=', 'cargo_pruebas.cargo_id')
            ->join('pruebas', 'pruebas.id', '=', 'cargo_pruebas.prueba_id')
            ->where('apuntado_evaluados.aspirante_id', '=', $aspirantes[$i]->id)
            ->select('pruebas.descripcion', 'apuntado_evaluados.id', 'pruebas.tipo_prueba')
            ->get();

            //en este creamos el {Aspirante, {PruebasRealizadas}}
            $aspirante = array($aspirantes[$i], $apuntado_evaluados);

            //Esto es lo que devuelve el metodo
            array_push($aspirantes_pruebas, $aspirante);//{{[ASPIRANTE],[PRUEBAS QUE HIZO EL ASPIRANTE]},
                                                        //{[ASPIRANTE],[PRUEBAS QUE HIZO EL ASPIRANTE]}}
            
        }
        //devolver apuntados_evaluados
        if($aspirantes_pruebas){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$aspirantes_pruebas
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }


    //CREAR PARA UNA VISTA INTERMEDIA EN DONDE EL
    //RECLUTADOR VEA LO QUE EL ASPIRANTE HIZO EN
    //CADA UNA DE LAS PRUEBAS
    public function ver_prueba($apuntado_evaluado_id)
    {
        $apuntado_evaluado = Apuntado_evaluado::find($apuntado_evaluado_id);
        $cargo_prueba = $apuntado_evaluado->cargo_prueba;
        $prueba = $cargo_prueba->prueba;
        $data;
        if($prueba->tipo_prueba == 'prueba_video')
        {
            $data = Prueba_video::find($prueba->id);;//Podria retornar tambien el id del aspirante
           
        }
        else if($prueba->tipo_prueba == 'cuestionario_psicotecnica')
        {
            $cuestionario_psicotecnica = Cuestionario_psicotecnicaController::find($prueba->id);

            $cuestionario_preguntas = Cuestionarios_preguntas::where('pruebas_cuestionarios_id', $cuestionario_psicotecnica->id)->get();
            $preguntas;
            $respuestas;
            $imagenes;
            foreach ($cuestionario_preguntas as $key => $cuestionario_pregunta) {
                $preguntas[$key] = Pregunta::find($cuestionario_pregunta->pregunta_id);
                $imagenes[$key] = Pregunta_imagen::where('pregunta_id', $preguntas[$key]->id)->get();
                $respuestas[$key] = Respuesta::where('pregunta_id', $preguntas[$key]->id)->where('aspirante_id', $apuntado_evaluado->aspirante_id)->get();
            }
            //LA DATA DEBERIA SER QUE SI CUESTIONARIO_PSICOTECNITA, PREGUNTAS, IMAGENES, RESPUESTAS

        }
        else
        {
            $cuestionario = Cuestionario::find($prueba->id);
            $cuestionario_preguntas = Cuestionarios_preguntas::where('pruebas_cuestionarios_id', $cuestionario->id)->get();

            $preguntas;
            $respuestas;

            foreach ($cuestionario_preguntas as $key => $cuestionario_pregunta) {
                $preguntas[$key] = Pregunta::find($cuestionario_pregunta->pregunta_id);
                $respuestas[$key] = Respuesta::where('pregunta_id', $preguntas[$key]->id)->where('aspirante_id', $apuntado_evaluado->aspirante_id)->get();
            }
            //LA DATA DEBERIA SER QUE SI CUESTIONARIO_PSICOTECNITA, PREGUNTAS, RESPUESTAS
        }
            
        if(is_object($data))
            {
                return response()->json([
                'estatus'=>'success',
                'code'=>'200',
                'data'=>$data
            
                ],200);
           } else
           {
                return response()->json([
                'estatus'=>'error',
                'message'=>'resource not found',
                'code'=>'404',
                ],404);
           }
    }

    //FUNCION DE UN RECLUTADOR
    public function evaluar_prueba(Request $request, $apuntado_evaluado_id)
    {
        $apuntado_evaluado = Apuntado_evaluado::find($apuntado_evaluado_id);
        $cargo_prueba = $apuntado_evaluado->cargo_prueba;
        $prueba = $cargo_prueba->prueba;

        if($tipo_prueba == "video")
        {
            $prueba->puntuacion = $request->puntuacion;
        }
        else
        {
            $preguntas = $request->preguntas;
            $respuestas = $request->respuestas;
            $puntaje_respuesta = 0;
            foreach ($respuestas as $key => $respuesta) {
                $respuesta->puntuacion = $request->puntuacion[$key];
                $puntaje_respuesta += $request->puntuacion[$key];
            }
            $puntuacion = $puntaje_respuesta / sizeof($respuestas);
            $prueba->puntuacion = $puntuacion;
            $apuntado_evaluado->fase = "Evaluado";
            $apuntado_evaluado->update();
            if($prueba->update())
            {
                return response()->json([
                    'status'=>'success',
                    'code'=>201,
                    'data'=> $prueba
                ],201);
            }
            else
            {
                $prueba->delete();
                //borrar video
                return response()->json([
                    'status'=>'error',
                    'code'=>404,
                    'message'=>''
                ],200);
            }

        }
        
    }
}
