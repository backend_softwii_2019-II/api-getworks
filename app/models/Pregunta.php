<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Pregunta extends Model
{
    protected $table='preguntas';
    //atributes
    
    protected $fillable = [
        'pregunta', 'puntuacion', "descripcion"
    ];
    //relacion de uno a muchos 
    public function imagenes()
    {
        return $this->hasMany('App\models\Pregunta_imagen');
    }
    public function respuestas(){
        return $this->hasMany('App\models\Respuesta');
    }

    //relacion de muchos a muchos
    public function Pruebas_cuestionarios(){
        return $this->belongsToMany('App\models\Pruebas_cuestionario');
    }
}
