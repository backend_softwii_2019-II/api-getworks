<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Oferta_trabajo extends Model
{
    protected $table='oferta_trabajos';
    //atributes
    
    protected $fillable = [
        'nombre_oferta', 'fechaApertura',"fechaCierre","ubicacion", "descripcion"
    ];

    //relaciones de muchos a muchos
    public function cargos()
    {
        return $this->belongsToMany('App\models\Cargo','oferta_cargos');
    }
    public function categorias()
    {
        return $this->belongsToMany('App\models\Categoria','oferta_categorias');
    }

    //relaciones de uno a muchos
    public function historial_ofertas()
    {
        return $this->hasMany('App\models\Historial_ofertas');
    }
    //inversa
    public function organizacion_reclutador(){
        return $this->belongsTo('App\models\OrganizacionReclutador');

    }
    public function reclutadores(){
        return $this->belongsTo('App\models\Reclutador');
    }
    
}
