<?php

namespace App\Http\Controllers;

use App\models\Oferta_trabajo;
use App\models\Reclutador;
use Illuminate\Http\Request;

class Oferta_trabajoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($data=Oferta_trabajo::all()->load('reclutadores')){
            return response()->json([
            'status'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else{
            return response()->json([
            'status'=>'error',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre_oferta'=>'required|string',
            'descripcion'=>'required',
            'fechaApertura'=>'required',
            'fechaCierre'=>'required',
            'ubicacion'=>'required',
            'reclutadores_id'=>'required',
        ]);
            
        $reclutador=Reclutador::where('id',$request->reclutadores_id)->first();
        if(!$reclutador)
            {
               return response()->json([
                       'success'=>false,
                       'message'=>'we can\'t find a reclutador  whith that id.',
                       'code'=>404,
               ],404);
           }
           $oferta=new Oferta_trabajo();
           $oferta->nombre_oferta=$request->nombre_oferta;
           $oferta->descripcion=$request->descripcion;
           $oferta->fechaApertura=$request->fechaApertura;
           $oferta->fechaCierre=$request->fechaCierre;
           $oferta->ubicacion=$request->ubicacion;
           $oferta->reclutadores_id=$request->reclutadores_id; 
           if( $oferta->save()){
            $oferta->reclutadores()->associate($reclutador);
   
               return response()->json([
                   'data'=> $oferta,
                   'success'=>true,
                   'message'=>'Successfully store processed',
                   'code'=>201,
               ],201);
              
           }else{
               return response()->json([
                   'status'=>'error',
                   'code'=>'404',
                   ],404);
           }
    }

    public function addCategorias(Request $request){
        
        $request->validate([
            'oferta_trabajo_id'=>'required',
            'categorias_id'=>'required'
        ]);
        
        $oferta=Oferta_trabajo::findOrFail($request->oferta_trabajo_id);
        if(is_object($oferta))
        {
            $oferta->categorias()->attach($request->categorias_id);
            return response()->json([
            'status'=>'success',
            'code'=>'200',
            'message'=>'categoria agrega correctamente'
        
            ],200);
       } else
       {
            return response()->json([
            'status'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }

    }
    public function show($id)
    {
        $data=Oferta_trabajo::findOrFail($id)->load('reclutadores','cargos','categorias');
        if(is_object($data))
        {
            return response()->json([
            'status'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else
       {
            return response()->json([
            'status'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre_oferta'=>'required|string',
            'descripcion'=>'required',
            'fechaApertura'=>'required',
            'fechaCierre'=>'required',
            'ubicacion'=>'required',
            'reclutadores_id'=>'required',
        ]);
            
        $reclutador=Reclutador::where('id',$request->reclutadores_id)->first();
        if(!$reclutador)
            {
               return response()->json([
                       'success'=>false,
                       'message'=>'we can\'t find a reclutador  whith that id.',
                       'code'=>404,
               ],404);
           }
           $oferta=Oferta_trabajo::findOrFail($id);
           $oferta->nombre_oferta=$request->nombre_oferta;
           $oferta->descripcion=$request->descripcion;
           $oferta->fechaApertura=$request->fechaApertura;
           $oferta->fechaCierre=$request->fechaCierre;
           $oferta->ubicacion=$request->ubicacion;
           $oferta->reclutadores_id=$request->reclutadores_id; 
           if( $oferta->save()){
            $oferta->reclutadores()->associate($reclutador);
   
               return response()->json([
                   'data'=> $oferta,
                   'success'=>true,
                   'message'=>'Successfully store processed',
                   'code'=>201,
               ],201);
              
           }else{
               return response()->json([
                   'status'=>'error',
                   'code'=>'404',
                   ],404);
           }
    }

    public function addCargos(Request $request){
        
        $request->validate([
            'cargo_id'=>'required',
            'oferta_trabajo_id'=>'required'
        ]);
        
        $oferta=Oferta_trabajo::findOrFail($request->oferta_trabajo_id);
        if(is_object($oferta))
        {
            $oferta->cargos()->attach($request->cargo_id);
            return response()->json([
            'status'=>'success',
            'code'=>'200',
            'message'=>'cargo agregado correctamente'
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }

    }
    public function destroy($id)
    {
        $data=Oferta_trabajo::findOrFail($id);
       
        if(is_object($data)){
           
            if($data->delete()){
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$data
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }
}
