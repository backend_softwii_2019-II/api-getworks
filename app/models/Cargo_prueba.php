<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CargoPrueba extends Model
{
    //relaciones 1 a muchos
    public function apuntado_evaluados(){
        return $this->hasMany('App\models\Apuntado_evaluado');
    }
    
     //relaciones de muchos amuchos
     public function cargos()
     {
         return $this->belongsToMany('App\models\Cargo');
     }
    public function pruebas()
    {
        return $this->belongsToMany('App\models\Prueba');
    }
}
