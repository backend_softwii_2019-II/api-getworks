<?php

namespace App\Http\Controllers;

use App\models\Aspirante;
use App\models\Docs_adjunt;
use App\models\Organizacion;
use Illuminate\Http\Request;

class DocsAdjuntController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($data=Docs_adjunt::all()){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $band=0;
        $request->validate([
            'nombre'=>'required|string',
            'tipoDocumento'=>'required',
            'formato'=>'required',
            'descripcion'=>'required',
            'organizacion_id'=>'required',
            'aspirante_id'=>'required',
            'file'=>'required|mimes:pdf|max:2048'
        ]);
        $docs=new Docs_adjunt();
        if($request->aspirante_id!="null"){

            $data=Aspirante::where('id',$request->aspirante_id)->first();
            $band=1;
            $docs->aspirante_id=$request->aspirante_id;

        }
        if($request->organizacion_id!="null"){
              $data=Organizacion::where('id',$request->organizacion_id)->first();
                $band=2;
                $docs->organizacion_id=$request->organizacion_id;
            }
         if(!$data)
         {
            return response()->json([
                    'success'=>false,
                    'message'=>'we can\'t find a organizacion or aspirante whith that id.',
                    'code'=>404,
            ],404);
         }



           
           
           $docs->descripcion=$request->descripcion;
           $docs->formato=$request->formato;
           $docs->tipoDocumento=$request->tipoDocumento;
           $docs->rutaDocumento="public/uploads";

           $filename=time().$request->nombre.'.'.$request->file->extension();
           $request->file->move(public_path('uploads'),$filename);
           $docs->nombre=$filename;
           return $filename;

           if( $docs->save()){
               if($band==1){

                $docs->Aspirante()->associate($data);
               }if($band==2){
                $docs->organizacion()->associate($data);

               }
               return response()->json([
                   'data'=> $docs,
                   'success'=>true,
                   'message'=>'Successfully store processed',
                   'code'=>201,
               ],201);
              
           }else{
               return response()->json([
                   'estatus'=>'error',
                   'code'=>'404',
                   ],404);
           }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Docs_adjunt::findOrFail($id);
        if(is_object($data))
        {
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $request->validate([
            'nombre'=>'required|string',
            'tipoDocumento'=>'required',
            'formato'=>'required',
            'descripcion'=>'required',
            'organizacion_id'=>'required',
            'aspirante_id'=>'required',
            'file'=>'required|mimes:pdf|max:2048'
        ]);
        return " ";
        
        $band=0;
        $docs= Docs_adjunt::findorFail($id);
        if($request->aspirante_id!="null"){

            $data=Aspirante::where('id',$request->aspirante_id)->first();
            $band=1;
            $docs->aspirante_id=$request->aspirante_id;

        }
        if($request->organizacion_id!="null"){
              $data=Organizacion::where('id',$request->organizacion_id)->first();
                $band=2;
                $docs->organizacion_id=$request->organizacion_id;
            }
         if(!$data)
         {
            return response()->json([
                    'success'=>false,
                    'message'=>'we can\'t find a organizacion or aspirante whith that id.',
                    'code'=>404,
            ],404);
         }

           
           $docs->descripcion=$request->descripcion;
           $docs->formato=$request->formato;
           $docs->tipoDocumento=$request->tipoDocumento;
           $docs->rutaDocumento="public/uploads";

          
           
            $filename=time().$request->nombre.$request->file->extension();
            

            $request->file->move(public_path('uploads'),$filename);
            
            $docs->nombre=$filename;
                    if( $docs->save()){
                         if($band==1){

                            $docs->Aspirante()->associate($data);
                         }if($band==2){
                            $docs->organizacion()->associate($data);

                            }
                        return response()->json([
                            'data'=> $docs,
                            'success'=>true,
                            'message'=>'Successfully store processed',
                            'code'=>201,
                             ],201);
              
                    }else{
                         return response()->json([
                            'estatus'=>'error',
                             'code'=>'404',
                            ],404);
                     }
           
           
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Docs_adjunt::findOrFail($id);
       
        if(is_object($data)){
           
            if($data->delete()){
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$data
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }


/*
    public function DocsAspirante()
    {
        return "";
        if($data=Docs_adjunt::all()->load('Aspirante')){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }
    public function DocsOrganizacion()
    {
        
        if($data=Docs_adjunt::all()->load('Organizacion')){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }
    public function showAspirante($id)
    {
        $data=Docs_adjunt::findOrFail($id)->load('Aspirante');
        if(is_object($data))
        {
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }
    public function showOrganizacion($id)
    {
        $data=Docs_adjunt::findOrFail($id)->load('Organizacion');
        if(is_object($data))
        {
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }*/
}
