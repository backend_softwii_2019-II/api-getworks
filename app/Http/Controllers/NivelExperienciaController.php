<?php

namespace App\Http\Controllers;

use App\models\Aspirante;
use App\models\Historial_experiencia;
use Illuminate\Http\Request;

class NivelExperienciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nivel=Historial_experiencia::all();
        if(is_object($nivel)){
            return response()->json([
                'status'=>'success',
                'code'=>'200',
                'data'=>$nivel
            ],200);
        }else
        {
            return response()->json([
                'status'=>'error',
                'code'=>'404',
                'message'=>'resource not found'
            ],404);
        }
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'cargo'=>'required|string',
            'nombre_organizacion'=>'required',
            'fecha_registro'=>'required',
            'duracion_empleo'=>'required',
            'aspirantes_id'=>'required'
        ]);
        $aspirante=Aspirante::where('id',$request->aspirantes_id)->first();
        if(!$aspirante)
            {
               return response()->json([
                       'success'=>false,
                       'message'=>'we can\'t find a aspirante  whith that id.',
                       'code'=>404,
               ],404);
           }
          
           $nivel=new Historial_experiencia();
           $nivel->cargo=$request->cargo;
           $nivel->nombre_organizacion=$request->nombre_organizacion;
           $nivel->fecha_registro=$request->fecha_registro;
           $nivel->duracion_empleo=$request->duracion_empleo;
           $nivel->descripcion=$request->descripcion;
           $nivel->aspirantes_id=$request->aspirantes_id; 
           if( $nivel->save()){
               $nivel->aspirante()->associate($aspirante);
   
               return response()->json([
                   'data'=> $nivel,
                   'success'=>true,
                   'message'=>'Successfully store processed',
                   'code'=>201,
               ],201);
              
           }else{
               return response()->json([
                   'estatus'=>'error',
                   'code'=>'404',
                   ],404);
           }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $nivel=Historial_experiencia::findOrFail($id);
       
        if(is_object($nivel)){
           return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$nivel
                    ],201);
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'cargo'=>'required|string',
            'nombre_organizacion'=>'required',
            'fecha_registro'=>'required',
            'duracion_empleo'=>'required',
            
        ]);
       
          
           $nivel=Historial_experiencia::findOrFail($id);
           $nivel->cargo=$request->cargo;
           $nivel->nombre_organizacion=$request->nombre_organizacion;
           $nivel->fecha_registro=$request->fecha_registro;
           $nivel->duracion_empleo=$request->duracion_empleo;
           $nivel->descripcion=$request->descripcion;
           if( $nivel->save()){
   
               return response()->json([
                   'data'=> $nivel,
                   'success'=>true,
                   'message'=>'Successfully store processed',
                   'code'=>201,
               ],201);
              
           }else{
               return response()->json([
                   'estatus'=>'error',
                   'code'=>'404',
                   ],404);
           }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nivel=Historial_experiencia::findOrFail($id);
       
        if(is_object($nivel)){
            if($nivel->delete())
            {
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$nivel
                    ],201);
            }else
            {
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
           
  
        }else{
            
            return response()->json([
                                    'status'=>'error',
                                    'message'=>'resource not found',
                                    'code'=>'404'
                        ],404);
            
        }
    
    }
}
