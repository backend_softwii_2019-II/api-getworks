<?php

namespace App\Http\Controllers;

use App\models\User;
use Illuminate\Http\Request;

class SuperUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($data=User::all()->where("rol","=","SuperUser")){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre'=>'required|string',
            'apellido'=>'required|string',
            'email'=>'required|email|unique:users',
            'nickname'=>'required|unique:users',
            'password'=>'required|min:8'
        ]); 
        $user=new User();
        $user->nombre=$request->nombre;
        $user->apellido=$request->apellido;
        $user->email=$request->email;
        $user->rol="SuperUser";
        $user->nickname=$request->nickname;
        $user->password=bcrypt($request->password);

        if($user->save()){
            return response()->json([
                'code'=>'201',
                'status'=>'success',
                'data'=>$user],201);
        }else{
            return response()->json([
                'status'=>'error',
                'message'=>'el usuario no se ha creado',
                'code'=>'404'
            ],404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user=User::where("id","=",$id)->where("rol","=","SuperUser")->first();
        if(is_object($user))
        {
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$user
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::findOrFail($id);
       
        if(is_object($user)){
           
            if($user->delete()){
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$user
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }
}
