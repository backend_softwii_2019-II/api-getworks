<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Evaluacion_seleccion extends Model
{
    protected $table='evaluacion_selecciones';
    //atributes
    
    protected $fillable = [
        'fecha_registro', 'tipo_seleccion', "descripcion"
    ];
    //relaciones inversas 
    public function Apuntado_evaluado(){
        return $this->belongsTo('App\models\Apuntado_evaluado');
    }
}
