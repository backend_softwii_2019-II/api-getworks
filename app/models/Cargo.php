<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $table='cargos';
    //atributes
    
    protected $fillable = [
        'nombre', 'nivel_experiencia', "tipo_empleo","pago_aproximado","modalidad","descripcion"
    ];

    //relaciones de muchos amuchos
    public function categorias()
    {
        return $this->belongsToMany('App\models\Categoria','cargo_categorias');
    }
    public function oferta_trabajos()
    {
        return $this->belongsToMany('App\models\Oferta_trabajo','oferta_cargos');
    }

    //relaciones 1 a muchos
    public function pruebas(){
        return $this->belongsToMany('App\models\Prueba','cargo_pruebas');
    }
}
