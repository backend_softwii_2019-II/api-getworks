<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Organizacion extends Model
{
    protected $table='organizaciones';
    //atributes
    
    protected $fillable = ['nombre','email','direccion','telefono','rif'
        
    ];


    protected $hiden=['password'];
    //relaciones de muchos a muchos nxm
   

    public function administradores()
    {
        return $this->hasMany('App\models\administradores');
    }

    //relaciones de uno amuchos 
    public function docs_adjunts(){
        return $this->hasMany('App\models\docs_adjunts');
    }
    public function reclutadores(){
        return $this->hasMany('App\models\Reclutador');
    }
}
