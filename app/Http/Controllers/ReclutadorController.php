<?php

namespace App\Http\Controllers;

use App\models\Organizacion;
use App\models\Reclutador;
use App\models\User;
use Illuminate\Http\Request;

class ReclutadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($data=Reclutador::all()->load('organizacion','user','oferta_trabajos')){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
         
        $request->validate([
            'user_id'=>'required',
            'organizacion_id'=>'required',
        ]);
      
        $user=User::where('id',$request->user_id)->first();
        $organizacion=Organizacion::where('id',$request->organizacion_id)->first();
        if(!$user || !$organizacion)
            {
               return response()->json([
                       'success'=>false,
                       'message'=>'we can\'t find a user or organizacion whith that id.',
                       'code'=>404,
               ],404);
           }
          
           $data= new Reclutador();

           $data->user_id=$request->user_id;
           $data->organizacion_id=$request->organizacion_id;
           if( $data->save()){
               $data->user()->associate($user);
               $data->organizacion()->associate($organizacion);
   
               return response()->json([
                   'data'=> $data,
                   'success'=>true,
                   'message'=>'Successfully store processed',
                   'code'=>201,
               ],201);
              
           }else{
               return response()->json([
                   'estatus'=>'error',
                   'code'=>'404',
                   ],404);
           }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Reclutador::findOrFail($id)->load('oferta_trabajo');
        if(is_object($data))
        {
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $request->validate([
            'user_id'=>'required',
            'organizacion_id'=>'required',
        ]);
        
        $user=User::where('id',$request->user_id)->first();
        $organizacion=Organizacion::where('id',$request->organizacion_id)->first();
        if(!$user || !$organizacion)
            {
               return response()->json([
                       'success'=>false,
                       'message'=>'we can\'t find a user or organizacion whith that id.',
                       'code'=>404,
               ],404);
           }
          
           $data=  Reclutador::findOrFail($id);

           $data->user_id=$request->user_id; 
           if( $data->save()){
               $data->user()->associate($user);
               $data->organizacion()->associate($organizacion);
   
               return response()->json([
                   'data'=> $data,
                   'success'=>true,
                   'message'=>'Successfully store processed',
                   'code'=>201,
               ],201);
              
           }else{
               return response()->json([
                   'estatus'=>'error',
                   'code'=>'404',
                   ],404);
           }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Reclutador::findOrFail($id);
       
        if(is_object($data)){
           
            if($data->delete()){
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$data
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }
}
