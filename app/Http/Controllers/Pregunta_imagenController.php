<?php

namespace App\Http\Controllers;

use App\models\Pregunta;
use App\models\Pregunta_imagen;
use Illuminate\Http\Request;

class Pregunta_imagenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($data=Pregunta_imagen::all()->load('preguntas')){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'file0'=>'required|image|mimes:jpg,jpeg,png,gif',
            'name'=>'required',
            'descripcion'=>'required',
            'formato'=>'required',
            'preguntas_id'=>'required'
            
            ]);
            $pregunta=Pregunta::where('id',$request->preguntas_id)->first();
            if(!$pregunta)
                {
                   return response()->json([
                           'success'=>false,
                           'message'=>'we can\'t find a pregunta  whith that id.',
                           'code'=>404,
                   ],404);
               }


        
        $image =$request->file('file0');
        if($image){
            $Img=new Pregunta_imagen();

            $Img->name=$request->name;
            $Img->descripcion=$request->descripcion;
            $Img->formato=$request->formato;
            $Img->preguntas_id=$request->preguntas_id;
            $Img->ruta="Storage/app/image";
            $image_name=time().$request->name;
            \Storage::disk('image')->put($image_name, \File::get($image));
            
            if($Img->save()){
                $Img->preguntas()->associate($pregunta);
                return response()->json([
                    'estatus'=>'success',
                    'code'=>'200',
                    'mesanje'=>'Imagen subida correctamente',
                    'data'=>$Img
        
                ],200); 
            }else{
                return response()->json([
                    'estatus'=>'error',
                    'message'=>'error ',
                    'code'=>'404',
                    ],404);
            }
        }else{
            return response()->json([
                'estatus'=>'error',
                'message'=>'resource not found',
                'code'=>'404',
                ],404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Pregunta_imagen::findOrFail($id);
        if(is_object($data))
        {
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'file0'=>'required|image|mimes:jpg,jpeg,png,gif',
            'name'=>'required',
            'descripcion'=>'required',
            'formato'=>'required',
            'preguntas_id'=>'required'
            
            ]);
            $pregunta=Pregunta::where('id',$request->preguntas_id)->first();
            if(!$pregunta)
                {
                   return response()->json([
                           'success'=>false,
                           'message'=>'we can\'t find a pregunta  whith that id.',
                           'code'=>404,
                   ],404);
               }


        
        $image =$request->file('file0');
        if($image){
            $Img=Pregunta_imagen::findOrFail($id);

            $Img->name=$request->name;
            $Img->descripcion=$request->descripcion;
            $Img->formato=$request->formato;
            $Img->preguntas_id=$request->preguntas_id;
            $Img->ruta="Storage/app/image";
            $image_name=time().$request->name;
            \Storage::disk('image')->put($image_name, \File::get($image));
            
            if($Img->save()){
                $Img->preguntas()->associate($pregunta);
                return response()->json([
                    'estatus'=>'success',
                    'code'=>'200',
                    'mesanje'=>'Imagen subida correctamente',
                    'data'=>$Img
        
                ],200); 
            }else{
                return response()->json([
                    'estatus'=>'error',
                    'message'=>'error ',
                    'code'=>'404',
                    ],404);
            }
        }else{
            return response()->json([
                'estatus'=>'error',
                'message'=>'resource not found',
                'code'=>'404',
                ],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Pregunta_imagen::findOrFail($id);
       
        if(is_object($data)){
           
            if($data->delete()){
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$data
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }
}
