<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{   
    protected $table='personas';
    protected $fillable = [
        "apellido", "fechadenacimiento", "nrocelular", "user_id"
    ];

    //relaciones de muchos a muchos nxm
    public function user()
    {
        return $this->belongsTo('App\models\User');
    }
    //relaciones de muchos a muchos nxm
    public function aspirante()
    {
        return $this->belongsToMany('App\models\Aspirante');
    }
    //relaciones de muchos a muchos nxm
    public function reclutador()
    {
        return $this->belongsToMany('App\models\Reclutador');
    }
    //relaciones de muchos a muchos nxm
    public function administrador()
    {
        return $this->belongsToMany('App\models\Administrador');
    }
    

}
