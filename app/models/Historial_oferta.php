<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Historial_oferta extends Model
{
    protected $table='historia_ofertas';
    //atributes
    
    protected $fillable = [
        'fecha_modificacion'
    ];

    //relacion inversa 
    public function oferta_trabajo()
    {
        return $this->belongsTo('App\models\Oferta_trabajo');
    }
}
