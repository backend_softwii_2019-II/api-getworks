<?php

namespace App\Http\Controllers;

use App\models\Organizacion;
use App\models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrganizacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        $data= Organizacion::all();
        if($data){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre'=> 'required',
            'rif'=>'required|unique:organizaciones',
            'email'=>'required|email|unique:organizaciones',
            'direccion'=>'required',
            'telefono'=>'required',
            'password'=>'required|min:8'
        ]); 


        $organizacion=new Organizacion();
        $organizacion->nombre=$request->nombre;
        $organizacion->rif=$request->rif;
        $organizacion->email=$request->email;
        $organizacion->direccion=$request->direccion;
        $organizacion->telefono=$request->telefono;
        $organizacion->password=$request->password;
        if($organizacion->save()){

                return response()->json([
                    'data'=> $organizacion,
                    'success'=>true,
                    'message'=>'Organizacion creada con exito',
                    'code'=>201,
                ],201);
               
        }else{
        return response()->json([
            'estatus'=>'error',
            'message'=>'La organizacion no pudo registrarse',
            'code'=>'404',
            ],404);
        }
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $organizacion=Organizacion::findOrFail($id);
 
        if(is_object($organizacion))
        {
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$organizacion
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function iniciar(Request $request )
    {
        $request->validate([
            'email'=>'required|email',
            'password'=>'required'
        ]);
        $band=0;
         $organizacion=Organizacion::all();
         foreach ($organizacion as $init){

           if(strcmp($init->email , $request->email)==0){
            $band+=1;
             
            if(strcmp($init->password,$request->password)==0){
                    $band+=1;
            }
           }  
         }
         if ($band==2) {
            // Authentication passed...
            return response()->json([
                'code'=>'201',
                'status'=>'success',
                'message'=>'inicio de sesion exitoso'
            ],201);
        }else{
            return response()->json([
                'status'=>'error',
                'message'=>'error email or password incorrecto',
                'code'=>'404'
            ],404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre'=> 'required',
            'rif'=>'required|unique:organizaciones',
            'email'=>'required|email|unique:organizaciones',
            'direccion'=>'required',
            'telefono'=>'required',
            'password'=>'required|min:8'
        ]); 

        $organizacion=Organizacion::findOrFail($id);
    
        if(is_object($organizacion))
        {    
            $organizacion=new Organizacion();
            $organizacion->nombre=$request->nombre;
            $organizacion->rif=$request->rif;
            $organizacion->email=$request->email;
            $organizacion->direccion=$request->direccion;
            $organizacion->telefono=$request->telefono;
            $organizacion->password=bcrypt($request->password); 
          

            if($organizacion->save()){
                return response()->json([
                    'estatus'=>'success',
                    'code'=>'200',
                    'data'=>$organizacion
        
                ],200); 
            }else{
                return response()->json([
                    'estatus'=>'error',
                    'message'=>'error update',
                    'code'=>'404',
                    ],404);
            }
            
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $organizacion=Organizacion::findOrFail($id);
        //$user=User::where('id',$organizacion->user_id)->with('organizaciones')->first();
        //$organizacion=Organizacion::with('Users')->where("organizaciones.id","=",$id)->first();
        
        if(is_object($organizacion)){

          
           
            if($organizacion->delete()){
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$organizacion
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }
}
