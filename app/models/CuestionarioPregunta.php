<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class CuestionarioPregunta extends Model
{
    //relaciones de muchos amuchos
    public function cuestionario()
    {
        return $this->belongsToMany('App\models\Cuestionario');
    }
    public function pregunta()
    {
        return $this->hasMany('App\models\Pregunta');
    }
}
