<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApuntadoEvaluadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apuntado_evaluados', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('fase')->nullable();
            $table->date('fecha_evaluacion')->nullable();
            $table->string('desempeno')->nullable();
            $table->string('puntaje')->nullable();
            $table->boolean('selecto')->nullable();
            $table->unsignedInteger('aspirante_id');
            $table->foreign('aspirante_id')->references('id')->on('aspirantes')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedInteger('cargo_prueba_id');
            $table->foreign('cargo_prueba_id')->references('id')->on('cargo_pruebas')->onDelete('restrict')->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apuntado_evaluados');
    }
}
