<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Cuestionario_encuesta extends Model
{
    protected $table='cuestionario_encuestas';
    //atributes
    
    public function Prueba_cuestionario(){
        return $this->belongsTo('App\models\Pruebas_cuestionario');
    }
}
