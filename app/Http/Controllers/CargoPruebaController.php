<?php

namespace App\Http\Controllers;

use App\CargoPrueba;
use App\models\Cargo;
use App\models\Prueba;
use Illuminate\Http\Request;

class CargoPruebaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($data=CargoPrueba::all()){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
            'cargo_id'=>'required|string',
            'prueba_id'=>'required',
           
        ]);
        $cargo=Cargo::where('id',$request->cargo_id)->first();
        $prueba=Prueba::where('id',$request->prueba_id)->first();
        if(!$cargo || !$prueba)
            {
               return response()->json([
                       'success'=>false,
                       'message'=>'we can\'t find a cargo or prueba whith that id.',
                       'code'=>404,
               ],404);
           }
          
           $cargoPrueba=new CargoPrueba();
         
           $cargoPrueba->cargo_id=$request->cargo_id; 
           $cargoPrueba->prueba_id=$request->prueba_id;
           if( $cargoPrueba->save()){
               $cargoPrueba->cargos()->associate($cargo);
               $cargoPrueba->pruebas()->associate($prueba);
   
               return response()->json([
                   'data'=> $cargoPrueba,
                   'success'=>true,
                   'message'=>'Successfully store processed',
                   'code'=>201,
               ],201);
              
           }else{
               return response()->json([
                   'estatus'=>'error',
                   'code'=>'404',
                   ],404);
           }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CargoPrueba  $cargoPrueba
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=CargoPrueba::findOrFail($id);
        if(is_object($data))
        {
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CargoPrueba  $cargoPrueba
     * @return \Illuminate\Http\Response
     */
    public function edit(CargoPrueba $cargoPrueba)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CargoPrueba  $cargoPrueba
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $request->validate([
            'cargo_id'=>'required|string',
            'prueba_id'=>'required',
           
        ]);
        $cargo=Cargo::where('id',$request->cargo_id)->first();
        $prueba=Prueba::where('id',$request->prueba_id)->first();
        if(!$cargo || !$prueba)
            {
               return response()->json([
                       'success'=>false,
                       'message'=>'we can\'t find a cargo or prueba whith that id.',
                       'code'=>404,
               ],404);
           }
          
           $cargoPrueba=CargoPrueba::findOrFail($id);
         
           $cargoPrueba->cargo_id=$request->cargo_id; 
           $cargoPrueba->prueba_id=$request->prueba_id;
           if( $cargoPrueba->save()){
               $cargoPrueba->cargos()->associate($cargo);
               $cargoPrueba->pruebas()->associate($prueba);
   
               return response()->json([
                   'data'=> $cargoPrueba,
                   'success'=>true,
                   'message'=>'Successfully store processed',
                   'code'=>201,
               ],201);
              
           }else{
               return response()->json([
                   'estatus'=>'error',
                   'code'=>'404',
                   ],404);
           }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CargoPrueba  $cargoPrueba
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=CargoPrueba::findOrFail($id);
       
        if(is_object($data)){
           
            if($data->delete()){
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$data
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }
}
