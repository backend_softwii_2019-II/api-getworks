<?php

namespace App\Http\Controllers;

use App\models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    
    
    public function index(){
        if($user=User::all()){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$user
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }
   
   
   
    public function signin(Request $request){
        $request->validate([
            'nombre'=>'required|string',
            'nickname'=>'required|string|unique:users',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:8'
        ]); 
        $user=new User();
        $user->nombre=$request->nombre;
        $user->nickname=$request->nickname;
        $user->email=$request->email;
        $user->password=bcrypt($request->password);

        if($user->save()){
            return response()->json([
                'code'=>'201',
                'status'=>'success',
                'data'=>$user],201);
        }else{
            return response()->json([
                'status'=>'error',
                'message'=>'el usuario no se ha creado',
                'code'=>'404'
            ],404);
        }
        
    }
    
    
    
    
    public function login(Request $request){
        $request->validate([
            'email'=>'required|email',
            'password'=>'required'
        ]);
         $credentials=$request->only('email','password');
         if (Auth::attempt($credentials)) {
            // Authentication passed...
            return response()->json([
                'code'=>'201',
                'status'=>'success',
                'message'=>'inicio de sesion exitoso'
            ],201);
        }else{
            return response()->json([
                'status'=>'error',
                'message'=>'error email or password incorrecto',
                'code'=>'404'
            ],404);
        }
    }
    
    
    
    
    
    
    
    public function update(Request $request,$id){
      
        $request->validate([
            'nombre'=>'required|string',
            'cedula'=>'required|string',
            'nrofijo'=>'required|string',
            'nickname'=>'required|string',
            'email'=>'required|unique:users,email,'.$id,
            'password'=>'required|min:8',
            'descripcion'=>'required|string',
            'direccion'=>'required|string',
        ]); 
        
        $user=User::findorfail($id);
        if(is_object($user))
        {   
            $user->nombre=$request->nombre;
            $user->nrodocumentoidentidad=$request->cedula;
            $user->nrofijo=$request->nrofijo;
            $user->descripcion=$request->descripcion;
            $user->direccion=$request->direccion;
            $user->nickname=$request->nickname;
            $user->email=$request->email;
            $user->password=bcrypt($request->password);

            if($user->save()){
                return response()->json([
                    'estatus'=>'success',
                    'code'=>'200',
                    'data'=>$user
        
                ],200); 
            }else{
                return response()->json([
                    'estatus'=>'error',
                    'message'=>'error update',
                    'code'=>'404',
                    ],404);
            }
            
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }

    }
   
   
   
   
    public function show ($id){
        $user=User::findOrFail($id);
        if(is_object($user))
        {
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$user
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }
        
    public function upload(Request $request){
        
       
       $request->validate([
           'file0'=>'required|image|mimes:jpg,jpeg,png,gif',
            'user_id'=>'required' 
       ]);
        
        $image =$request->file('file0');
        if($image){
            $user=User::findorFail($request->user_id);
            $image_name=time().$image->getClientOriginalName();
            \Storage::disk('user')->put($image_name, \File::get($image));
            $user->imagen=$image_name;
            if($user->save()){
                return response()->json([
                    'estatus'=>'success',
                    'code'=>'200',
                    'mesanje'=>'Imagen subida correctamente'
        
                ],200); 
            }else{
                return response()->json([
                    'estatus'=>'error',
                    'message'=>'error ',
                    'code'=>'404',
                    ],404);
            }
        }else{
            return response()->json([
                'estatus'=>'error',
                'message'=>'resource not found',
                'code'=>'404',
                ],404);
        }
        
        
    }
    public function getimage($id){

        
        
        $user=User::findorFail($id);
        if(is_object($user)){
            $isset=\Storage::Disk('user')->exists($user->imagen);
             if($isset){

                 $file=  \Storage::disk('user')->get($user->imagen);
 


                        return new Response($file,200);


             }else{
                 return response()->json([
                 'estatus'=>'error',
                    'message'=>'la imagen no existe',
                     'code'=>'404',
                 ],404);
                 }
        }else{
            return response()->json([
                'estatus'=>'error',
                   'message'=>'usuario no encontrado',
                    'code'=>'404',
                ],404);
        }

        
      
         
         
         
     }
    
    
    
    
    
    
    
    
    
    
    
    public function destroy($id){
        $user=User::findOrFail($id);
       
        if(is_object($user)){
           
            if($user->delete()){
                return response()->json([
                    'code'=>'200',
                    'status'=>'success',
                    'data'=>$user
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }
}

