<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCargosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargos', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('nombre')->nullable();
            //$table->enum('experiencia',['none','años'])->comment('experiencia: none=nada años=cantidad');
            $table->string('nivel_experiencia')->nullable();
            $table->enum('tipo_empleo',['completo','parcial'])->comment('parcial o jornada completa')->nullable();
            $table->string('pago_aproximado')->nullable();
            $table->enum('modalidad',['presencial','telework','freelance'])->comment('modalidad de trabajo remoto o presencial')->nullable();
            $table->string('descripcion')->nullable();
            //$table->enum('sectores',['Marketin y publicidad','Servicios y tecnologías de la información','Software','Seguridad del ordenador y de las redes'])->comment('sectores de trabajo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargos');
    }
}
