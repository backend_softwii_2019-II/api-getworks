<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table='categorias';
    //atributes
    
    protected $fillable = [
        'nombre','descripcion'
    ];

    //relaciones de muchos amuchos
    public function cargos()
    {
        return $this->belongsToMany('App\models\Cargo','cargo_categorias');
    }
    public function oferta_trabajos()
    {
        return $this->belongsToMany('App\models\Oferta_trabajo');
    }
}
