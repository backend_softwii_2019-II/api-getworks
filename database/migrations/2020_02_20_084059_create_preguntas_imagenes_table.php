<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreguntasImagenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preguntas_imagenes', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('name')->nullable();
            $table->string('descripcion')->nullable();
            $table->string('formato')->nullable();
            $table->string('ruta')->nullable();
            $table->unsignedInteger('preguntas_id')->nullable();
            $table->foreign('preguntas_id')->references('id')->on('preguntas')->onDelete('restrict')->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preguntas_imagenes');
    }
}
