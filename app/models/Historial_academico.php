<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Historial_academico extends Model
{
    protected $table='historial_academicos';
    //atributes
    
    protected $fillable = [
        'nivel_academico', 'institucion', 'fecha_registro',"descripcion","aspirantes_id"
    ];



    //relacion inversa 
    public function aspirante()
    {
        return $this->belongsTo('App\models\Aspirante');
    }
}
