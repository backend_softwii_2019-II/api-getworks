<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvaluacionSeleccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluacion_selecciones', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('descripcion')->nullable();
            $table->date('fecha_registro')->nullable();
            $table->string('tipo_seleccion')->nullable();
            $table->unsignedInteger('apuntado_evaluados_id');
            $table->foreign('apuntado_evaluados_id')->references('id')->on('apuntado_evaluados')->onDelete('restrict')->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluacion_selecciones');
    }
}
