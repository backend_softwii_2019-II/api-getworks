<?php

namespace App\Http\Controllers;

use App\models\Organizacion;
use App\models\Aspirante;
use App\models\Administrador;
use App\models\Persona;
use App\models\User;
use Illuminate\Http\Request;


class AdministradorController extends Controller
{

	public function index()
    {

        $data = Administrador::with(['persona.user','organizacion'])->get();

        if($data){
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else{
            return response()->json([
            'estatus'=>'error',
            'code'=>'404',
            ],404);
       }
    }

	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id'=>'required',
            'organizacion_id'=>'required',
            'apellido'=>'required|string',
            'fechadenacimiento'=>'required|string',
            'nrocelular'=>'required|string',
        ]);

        $organizacion= Organizacion::findOrFail($request->organizacion_id);
        $user = User::findOrFail($request->user_id);

        if(!$organizacion)
             {
                return response()->json([
                        'success'=>false,
                        'message'=>'No existe una empresa con ese id',
                        'code'=>404,
                ],404);
            }
        $usuarioOrganizacion = Organizacion::where("user_id","=",$user->id)->first();
        if(is_object($usuarioOrganizacion))
             {
                return response()->json([
                        'success'=>false,
                        'message'=>'Este usuario es una organizacion',
                        'code'=>404,
                ],404);
            }
        if(!$user)
             {
                return response()->json([
                        'success'=>false,
                        'message'=>'No existe un usuario con ese id',
                        'code'=>404,
                ],404);
            }
        
        $personaCreada = Persona::where("user_id","=",$user->id)->first();
        if(is_object($personaCreada))
        {   
            if (Administrador::where("persona_id","=",$personaCreada->id)) {
                return response()->json([
                        'success'=>false,
                        'message'=>'Fallo al crear por que esta persona ya era un administrador',
                        'code'=>404,
                ],404);
            }
            $persona = $personaCreada;
        }else{
            $persona=new Persona();
            $persona->apellido = $request->apellido;
            $persona->fechadenacimiento = $request->fechadenacimiento;
            $persona->nrocelular = $request->nrocelular;
            $persona->user_id = $user->id;

            if(!$persona->save())
             {
                return response()->json([
                        'success'=>false,
                        'message'=>'Fallo por datos personales incorrectos',
                        'code'=>404,
                ],404);
            }
        }

        $administrador = new Administrador();
        $administrador->organizacion_id=$organizacion->id;
        $administrador->persona_id=$persona->id;

        if($administrador->save()){
            $user->rol = "Administrador";
            $user->save();
            $persona->user()->associate($user);
            $administrador->persona()->associate($persona);
            $administrador->organizacion()->associate($organizacion);


            return response()->json([
                'success'=>true,
                'data'=>$administrador,
                'message'=>'Successfully store processed',
                'code'=>201,
            ],201);
           
        }else{
            return response()->json([
                'estatus'=>'error',
                'code'=>'404',
                ],404);
        }
    }

    public function show($id)
    {
        $data = Administrador::with(['persona.user','organizacion'])->where('id','=',$id)->get();
        if(is_object($data))
        {
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }

    function update(Request $request,$id){
        $request->validate([
            'user_id'=>'required',
            'apellido'=>'required|string',
            'fechadenacimiento'=>'required|string',
            'nrocelular'=>'required|string',
        ]);
       
        $administrador= Administrador::where('aspirantes.id',$id)->with('persona.user')->first();


        $administrador->persona->apellido=$request->apellido;
        $administrador->persona->fechadenacimiento = $request->fechadenacimiento;
        $administrador->persona->nrocelular = $request->nrocelular;
        $administrador->persona->user->nombre=$request->nombre;
        $administrador->persona->user->nrodocumentoidentidad=$request->cedula;
        $administrador->persona->user->nrofijo=$request->nrofijo;
        $administrador->persona->user->descripcion=$request->descripcion;
        $administrador->persona->user->direccion=$request->direccion;
        $administrador->persona->user->nickname=$request->nickname;
        $administrador->persona->user->email=$request->email;

        if( $administrador->save()){
         return response()->json([
                'data'=> $administrador,
                'success'=>true,
                'message'=>'Successfully update processed',
                'code'=>201,
            ],201);
           
        }else{
            return response()->json([
                'estatus'=>'error',
                'code'=>'404',
                ],404);
        }
    }

    public function filtrar_PorOrganizacion($id_organizacion)
    {
        $data = Administrador::with(['persona.user','organizacion'])->where('organizacion_id','=',$id_organizacion)->get();
        if(is_object($data))
        {
            return response()->json([
            'estatus'=>'success',
            'code'=>'200',
            'data'=>$data
        
            ],200);
       } else
       {
            return response()->json([
            'estatus'=>'error',
            'message'=>'resource not found',
            'code'=>'404',
            ],404);
       }
    }

    public function cambiarDeOrgranizacion(Request $request){
        $administrador = Administrador::findOrFail($request->id_administrador);
        $organizacionOld = Organizacion::findOrFail($request->id_organizacionOld);
        $organizacionNew = Organizacion::findOrFail($request->id_organizacionNew);

        if(is_object($organizacionNew) and is_object($administrador) and is_object($organizacionOld)){
            $administrador->organizacion_id = $organizacionNew->id;
            if ($administrador->save()) {
                $administrador->organizacion()->associate($organizacionNew);
                return response()->json([
                'success'=>true,
                'data'=>$administrador,
                'message'=>'Successfully store processed',
                'code'=>201,]);
            }else{
                return response()->json([
                'estatus'=>'error',
                'message'=>'Fallo al guardar de nuestro lado, intente de nuevo',
                'code'=>'404',
                ],404);
            }
        }else{
            return response()->json([
            'estatus'=>'error',
            'message'=>'Fallo de datos, Revise los datos y vuelva a intentarlo',
            'code'=>'404',
            ],404);
        }
    }


    public function destroy($id)
    {
        $administrador = Administrador::with(['persona.user','organizacion'])->where('id','=',$id)->first();
       
        if(is_object($administrador)){
            if($administrador->delete()){
                $aspirante = new Aspirante();
                $administrador->persona->user->rol="Aspirante";
                $administrador->persona->user->save();
                $aspirante->persona_id = $administrador->persona_id;
                $aspirante->save();
                return response()->json([
                    'code'=>'200',
                    'status'=>'success'
                    ],201);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=>'error deleting',
                    'code'=>'404'
                ],404);
            }
  
        }else{
            return response()->json([
                        'status'=>'error',
                        'message'=>'resource not found',
                        'code'=>'404'
            ],404);
        }
    }
}
